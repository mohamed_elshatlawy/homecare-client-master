import 'package:flutter/widgets.dart';

class CustomColor{
  static final primaryColor=Color.fromRGBO(48, 83, 121, 1);
  static final secondartColor=Color.fromRGBO(107, 195, 174, 1);
  static final thirdColor=Color.fromRGBO(156, 178, 194, 1);
  static final fourthColor=Color.fromRGBO(113, 145, 175, 1);
}