import 'package:flutter/material.dart';
import 'package:homecare/home.dart';
import 'package:homecare/providers/user.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';
import 'package:provider/provider.dart';
import 'dart:convert';

class Locations extends StatefulWidget {
  final String city;
  Locations({this.city, Key key}) : super(key: key);
  _LocationsState createState() => _LocationsState();
}

class _LocationsState extends State<Locations> {
  var data;

  Future<String> getLocations() async {
    var response = await http.get(
        Uri.encodeFull("http://delivery.digitalharbor.me//api/city/" +
            widget.city +
            "?locale=ar"),
        headers: {"Accept": "application/json"});
    setState(() {
      data = json.decode(response.body);
    });
    return "Success";
  }

  Widget getList() {
    if (data == null || data.length < 1) {
      return Container(
        height: MediaQuery.of(context).size.height * 0.4,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    return Container(
      height: MediaQuery.of(context).size.height * 0.4,
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
        itemCount: data?.length,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.only(left: 8, right: 8, top: 5),
            child: getListItem(index),
          );
        },
      ),
    );
  }

  Widget getListItem(int i) {
    final user = Provider.of<UserModel>(context);

    if (data == null || data.length < 1) return null;

    return InkWell(
      child: Container(
        height: 60,
        width: MediaQuery.of(context).size.width * 0.9,
        child: Card(
          color: Color.fromRGBO(58, 95, 126, 1),
          child: Padding(
            padding: EdgeInsets.all(4),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  data[i]['title'].toString(),
                  textAlign: TextAlign.center,
                  textDirection: TextDirection.rtl,
                  style: TextStyle(
                    fontSize: 14,
                    fontFamily: "ar",
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      onTap: () {
        user.locationID = data[i]['id'].toString();
        user.locationTitle = data[i]['title'].toString();

        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Home()));
      },
    );
  }

  @override
  void initState() {
    super.initState();
    getLocations();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          SizedBox(
            height: 100,
          ),
          Image.asset(
            "assets/location.png",
            width: MediaQuery.of(context).size.width / 3,
          ),
          Text(
            "حدد منطقتك",
            style: TextStyle(
                fontSize: 22,
                fontFamily: "ar",
                color: Color.fromRGBO(58, 95, 126, 1)),
          ),
          getList(),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    ));
  }
}
