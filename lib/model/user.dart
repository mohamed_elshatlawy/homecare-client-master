import 'package:flutter/foundation.dart';

class SocialUser {
  String name;
  String mail;

  String imgURL;

  String token;

  SocialUser(
      {@required this.name,
      @required this.mail,
      @required this.imgURL,
      @required this.token});
}

class User {
  String name;
  String mail;
  String password;
  String mobile;
  String token;
  String userType;

  User(
      {@required this.name,
      @required this.mail,
      @required this.mobile,
      @required this.password});
  User.fromMap(Map<String, dynamic> json) {
    this.mail = json['email'];
    this.name = json['name'];
    this.mobile = json['mobile'];
    this.token = "Bearer ${json['token']}";
    this.userType = json['type'];
  }
  Map<String, dynamic> toMap() {
    return {
      'name': this.name,
      'email': this.mail,
      'password': this.password,
      'mobile': this.mobile
    };
  }
}
