import 'dart:convert';
import 'dart:io';
import 'package:path/path.dart';
import 'package:async/async.dart';
import 'package:dio/dio.dart' as dio;
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';

Future<String> checkpromoCode(String code) async {
  String baseURL =
      "https://delivery.digitalharbor.me//api/coupon/$code?locale=ar";

  var client = http.Client();
  http.Response response = await client.get(baseURL);

  if (response.statusCode == 200) {
    var json = jsonDecode(response.body);

    String date = json['expiry_date'];

    var temp = DateTime.now().toUtc();
    var d1 = DateTime.utc(temp.year, temp.month, temp.day);
    var d2 = DateTime.utc(
        int.parse(date.split('-')[0]),
        int.parse(date.split('-')[1]),
        int.parse(date.split('-')[2])); //you can add today's date here
    if (d1.isAfter(d2)) {
      print('expi');
      return 'expired';
    } else {
      print('done');
      return 'availabe';
    }
  }
  return 'error';
}



Future<dynamic> updateUserInfo(
    String fieldValue,fieldKey, String token) async {
  String baseURL = "https://delivery.digitalharbor.me//api/profile";
  var client = http.Client();
  http.Response response = await client.put(baseURL,
      body: {fieldKey: fieldValue,},
      headers: {'Authorization': token});
  print(response.body);
  if (response.statusCode == 200) {
    return 'تم تعديل البيانات بنجاح';
  }
  return 'حدث خطأ في تحديث البيانات حاول مرة اخر';
}

Future<dynamic> updateUserImage(File file, String token) async {
  String baseURL = "https://delivery.digitalharbor.me//api/profile/image";
   Map<String, String> headers = {
      HttpHeaders.authorizationHeader: "Bearer " +token,
      'Content-Type': 'multipart/form-data'
    };
    var formData = dio.FormData.fromMap({
      'image':await dio.MultipartFile.fromFile(
        file.path,
          contentType: MediaType('image','jpeg')
         
        ),
          });
    print('FormData:${formData}');
    dio.Response response;
  
    try {
      response = await dio.Dio()
          .post(baseURL, data: formData, options: dio.Options(headers: headers));
      if (response.statusCode == 200) {
         String convertedResp = response.data.toString();
      print('MyResp:$convertedResp');
        return 'تم ارسال الطلب بنجاح';
      }
      print('Status:${response.statusCode}');

      String convertedResp = response.data.toString();
      print('MyResp:$convertedResp');
    } on dio.DioError catch (e) {
      print('ErrorReson:${e.response.statusMessage}');
      print('Eee:${e.toString()}');

      return e.response.statusMessage;
//      print('statusMsg:${response.statusMessage}');
}
}


Future<dynamic> sendToken(String fcmToken,String userToken) async {
  String baseURL="https://delivery.digitalharbor.me//api/post-login?locale=ar";

  var client=http.Client();
  Map<String,String> params={
    'device_token':fcmToken
  };
  Map<String,String> header={
    'Authorization':'Bearer $userToken'
  };
  http.Response response=await client.post(baseURL,body: params,headers:header );

  if(response.statusCode==200){
    print('Success');
  }else{
    print(response.statusCode);

    print(response.body);
  }
}

Future<dynamic> confirmOrderPrice(int id,String token) async {
  print('reqId:$id');
  print('Token:$token');
  String baseURL="https://delivery.digitalharbor.me//api/request/$id?locale=ar";
  Map<String,String> header={
    'Authorization':token
  };

  var client=http.Client();

  http.Response response=await client.put(baseURL,headers: header);

  print(response.body);
  if(response.statusCode==200){
    return 'تم ارسال الطلب بنجاح';
  }

  return null;
  
}