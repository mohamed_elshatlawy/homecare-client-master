import 'dart:convert';

import 'package:homecare/model/user.dart';
import 'package:http/http.dart' as http;

import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'package:http/http.dart' as http;

Future<dynamic> userLogin(String mail, String pass) async {
  String baseURL = "https://delivery.digitalharbor.me//api/login?locale=ar";
  var client = http.Client();
  http.Response response =
      await client.post(baseURL, body: {'email': mail, 'password': pass});

  if (response.statusCode == 401)
    return 'الايميل أو كلمة المرور خطأ';
  else if (response.statusCode == 200) {
    var jsonResponse = jsonDecode(response.body);
    return User.fromMap(jsonResponse);
  } else if (response.statusCode == 500) {
    return 'فشل في الاتصال بالخادم';
  }
}

Future<dynamic> userSignUp(User user) async {
  String baseURL = "https://delivery.digitalharbor.me//api/register?locale=ar";
  var client = http.Client();
  http.Response response =
      await client.post(baseURL, body: user.toMap());

  if (response.statusCode == 422)
    return 'فشل في الاتصال بالخادم';
  else if (response.statusCode == 200) {
    var jsonResponse = jsonDecode(response.body);
    return User.fromMap(jsonResponse);

  } else if (response.statusCode == 500) {
    return 'فشل في الاتصال بالخادم';
  }
}

Future<dynamic> loginFb() async {
  final facebookLogin = FacebookLogin();
  final result = await facebookLogin.logIn(['email']);

  switch (result.status) {
    case FacebookLoginStatus.loggedIn:
      //_sendTokenToServer(result.accessToken.token);
      // _showLoggedInUI();
      final graphResponse = await http.get(
          'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${result.accessToken.token}');
      final profile = jsonDecode(graphResponse.body);

      var url = 'https://delivery.digitalharbor.me//api/social/facebook';

      var response = await http.post(url, body: {
        'locale': 'ar',
        'token': result.accessToken.token,
        'id': profile['id'],
        'name': profile['name'],
        'email': profile['email']
      });

      var user = jsonDecode(response.body);

      return User.fromMap(user);

      break;
    case FacebookLoginStatus.cancelledByUser:
      return 'تم الغاء التسجيل';

      break;
    case FacebookLoginStatus.error:
      print(result.errorMessage);

      return result.errorMessage;
      break;
  }
}

Future<dynamic> googleLogin() async {
  GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: [
      'email',
      
    ],
  );

  GoogleSignInAccount acc = await _googleSignIn.signIn();

  var auth = await acc.authentication;

  var url = 'https://delivery.digitalharbor.me//api/social/google';
  var response = await http.post(url, body: {
    'locale': 'ar',
    'token': auth.idToken,
    'id': acc.id,
    'name': acc.displayName,
    'email': acc.email
  });

  var userJson = jsonDecode(response.body);
  return User.fromMap(userJson);
}
