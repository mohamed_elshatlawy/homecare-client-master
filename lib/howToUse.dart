import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class HowToUse extends StatefulWidget {
  _TermsState createState() => _TermsState();
}

VideoPlayerController _controller;

class _TermsState extends State<HowToUse> {
  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(
        'http://www.sample-videos.com/video123/mp4/720/big_buck_bunny_720p_20mb.mp4')
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        if(mounted){
          setState(() {
            
          });
        }
      })..play();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      
          child: Scaffold(
        body: Container(
          margin: EdgeInsets.only(left: 22, right: 22, top: 40),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 10,
              ),
              Row(
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      _controller.pause();
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Icons.arrow_back_ios,
                      size: 20,
                    ),
                  )
                ],
              ),
              Image.asset("assets/terms.png", width: 150),
              Text(
                "كيفية الأستخدام",
                style: TextStyle(
                    fontSize: 20,
                    fontFamily: "ar",
                    color: Color.fromRGBO(58, 95, 126, 1)),
              ),
              SizedBox(
                height: 10,
              ),
              Expanded(
                child: Directionality(
                  textDirection: TextDirection.rtl,
                  child: Scrollbar(
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          Text(
                            'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما',
                            textAlign: TextAlign.right,
                            style: TextStyle(
                                fontFamily: "ar",
                                fontSize: 12,
                                color: Color.fromRGBO(117, 117, 117, 1)),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            width: double.infinity,
                            color: Colors.grey[200],
                            height: 200,
                            child: _controller.value.initialized
                                ? AspectRatio(
                                    aspectRatio: _controller.value.aspectRatio,
                                    child: VideoPlayer(_controller),
                                  )
                                : Container(
                                  child: Center(
                                    child: CircularProgressIndicator(),
                                  ),
                                ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 40,
              ),
            ],
          ),
        ),
      ), onWillPop: () {

        _controller.pause();
          Navigator.pop(context);
      },
    );
  }
}
