import 'package:flutter/material.dart';
import 'package:homecare/menu.dart';
import 'package:homecare/order/orders.dart';

class OrderConnfirm extends StatefulWidget {
  final String type;
  OrderConnfirm({this.type, Key key}) : super(key: key);

  @override
  _OrderConnfirmState createState() => _OrderConnfirmState();
}

TextEditingController problemController = new TextEditingController();

class _OrderConnfirmState extends State<OrderConnfirm> {
  static final _formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();

    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        key: _scaffoldKey,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
                  child: AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            leading: InkWell(
              child: Icon(
                Icons.chevron_left,
                color: Color.fromRGBO(48, 83, 121, 1),
                size: 35,
              ),
              onTap: () {
                // TODO: function to return back to the map page
              },
            ),
            title: Text(
              "طلب الخدمة",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 16,
                  fontFamily: "ar",
                  color: Color.fromRGBO(58, 95, 126, 1)),
            ),
            centerTitle: true,
            actions: <Widget>[
              InkWell(
                child: Image.asset(
                  "assets/menu.png",
                  width: 25,
                  height: 18.3,
                ),
                onTap: () {
                  _scaffoldKey.currentState.openEndDrawer();
                },
              )
              ,SizedBox(width: 20,)
            ],
          ),
        ),
        endDrawer: Drawer(child: Menu()),
        body: Builder(builder: (context) {
          return Stack(
            children: <Widget>[
              Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Container(
                            width: 189,
                            height: 189,
                            child: Image.asset("assets/confirm.png")),
                        SizedBox(
                          height: 30,
                        ),
                        Text(
                          "لقد تم ارسال طلبك بنجاح",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 20,
                              fontFamily: "ar",
                              color: Color.fromRGBO(58, 95, 126, 1)),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "نشكرك علي ثقتك بنا",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 16,
                              fontFamily: "rb",
                              color: Color.fromRGBO(145, 145, 145, 1)),
                        ),
                        Text(
                          "ونتمني دائما ان نكون في خدمتك",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 16,
                              fontFamily: "rb",
                              color: Color.fromRGBO(145, 145, 145, 1)),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 156,
                    ),
                    SizedBox(
                      height: 40,
                    )
                  ],
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(bottom: 40,left: 20,right: 20),
                  height: 48,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.all(
                      const Radius.circular(8.0),
                    )),
                    color: Color.fromRGBO(58, 95, 126, 1),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Orders()));
                    },
                    child: Text(
                      "طلباتي",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontFamily: "ar"),
                    ),
                  ),
                ),
              )
            ],
          );
        }),
      ),
    );
  }
}
