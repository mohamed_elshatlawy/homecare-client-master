import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:homecare/backEnd/service.dart';
import 'package:homecare/color/myColor.dart';
import 'package:homecare/menu.dart';
import 'package:homecare/order/confirm.dart';
import 'package:homecare/providers/user.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:file_picker/file_picker.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as prefix0;
import 'package:mime/mime.dart';
import 'package:video_player/video_player.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:dio/dio.dart' as dio;
import 'package:path/path.dart';
import 'package:http_parser/http_parser.dart';

class OrderAttach extends StatefulWidget {
  final String id;
  final String price;
  final String subID;

  OrderAttach({this.id, this.price, this.subID, Key key}) : super(key: key);

  @override
  _OrderAttachState createState() => _OrderAttachState(id, subID);
}

TextEditingController problemController = TextEditingController();

class _OrderAttachState extends State<OrderAttach> {
  static final _formKey = new GlobalKey<FormState>();
  Map<String, String> filesPaths;
  Map<String, String> filesValues;
  String fileBody, servID, subID;
  TextEditingController descripProblem = TextEditingController();

  _OrderAttachState(this.servID, this.subID);
  @override
  void initState() {
    super.initState();
    // if(filesValues !=null)
    // filesValues.clear();
  }

  String body;
  Map valueMap;
  List<Map<String, String>> fileTypes = [];

  Future<List<dio.MultipartFile>> getMultipartFiles() async {
    List<dio.MultipartFile> items = [];

    print('insideMultiPart:$fileTypes');
    for (int i = 0; i < filesValues.values.length; i++) {
      items.add(
        await dio.MultipartFile.fromFile(filesValues.values.elementAt(i),
            contentType: MediaType(fileTypes[i].keys.elementAt(0),
                fileTypes[i].values.elementAt(0))),
      );
    }
    print('ItemsLen:${items.length}');
    return items;
  }

  Future<dynamic> _makePostRequest(String servID, String subID) async {
    // print(filesValues);
    final user = Provider.of<UserModel>(this.context);

// Get base file name

    String url = 'https://delivery.digitalharbor.me//api/request/store2';
    Map<String, String> headers = {
      HttpHeaders.authorizationHeader: "Bearer " + user.userToken,
      'Content-Type': 'multipart/form-data'
    };

    //String copoun = (pinController.text == null) ? '' : pinController.text;
    //print('Coup:$copoun');

    var formData = dio.FormData.fromMap({
      'subscription_id': subID,
      'service_request_id': servID,
      'payment_method': 'cod',
      'notes': problemController.text,
      'coupon': finalCodeVal,

      'media': (fileTypes.length > 0) ? await getMultipartFiles() : []
      //[
      /*await dio.MultipartFile.fromFile(
          filesValues.values.elementAt(0),
          contentType: MediaType('image','jpeg')
         
        ),
       
      await dio.MultipartFile.fromFile(
          filesValues.values.elementAt(1),
           contentType: MediaType('image','jpeg')
         
        ),
        for()*/

      //]
    });
    
    print('FormData:${formData}');
    dio.Response response;
    try {
      response = await dio.Dio()
          .post(url, data: formData, options: dio.Options(headers: headers));
      if (response.statusCode == 200) {
        String convertedResp = response.data.toString();
        print('MyResp:$convertedResp');
        return 'تم ارسال الطلب بنجاح';
      }
      print('Status:${response.statusCode}');

      String convertedResp = response.data.toString();
      print('MyResp:$convertedResp');
    } on dio.DioError catch (e) {
      print('ErrorReson:${e.response.statusMessage}');
      print('Eee:${e.toString()}');

      return e.response.statusMessage;
//      print('statusMsg:${response.statusMessage}');

    }
  }

  String pinCodeValue = '', finalCodeVal = '';

  bool showBottomSheet = false;
  VideoPlayerController _controller;
  AudioPlayer audioPlayer;
  int index = -1;
  List<Widget> getGridImages() {
    List<Widget> images = [];
    fileTypes.clear();
    index = -1;
    if (filesValues != null)
      filesValues.forEach((k, v) {
        String mimeStr = lookupMimeType(v);
        var fileType = mimeStr.split('/');
        index++;
        fileTypes.add({fileType[0]: fileType[1]});
        print('file type ${fileType}');
        print('lenghFileype:$fileTypes');

        images.add(InkWell(
          onTap: () async {
            if ((fileType[0] == 'video')) {
              _controller = VideoPlayerController.file(File(v));
              await _controller.initialize();
              _controller.play();
            } else if (fileType[0] == 'audio') {
              audioPlayer = AudioPlayer();
              audioPlayer.play(v, isLocal: true);
            }

            showDialog(
                barrierDismissible: false,
                context: this.context,
                builder: (context) {
                  return Dialog(
                    backgroundColor: Colors.transparent,
                    elevation: 0,
                    child: Container(
                      width: double.infinity,
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: (fileType[0] == 'video')
                                ? _controller.value.initialized
                                    ? AspectRatio(
                                        aspectRatio:
                                            _controller.value.aspectRatio,
                                        child: VideoPlayer(_controller),
                                      )
                                    : Container()
                                : (fileType[0] == 'audio')
                                    ? Image.asset(
                                        'assets/playIcon.png',
                                        width: 100,
                                        height: 100,
                                      )
                                    : ClipRRect(
                                        borderRadius: BorderRadius.circular(8),
                                        child: Image.file(
                                          File(
                                            v.toString(),
                                          ),
                                          fit: BoxFit.fill,
                                          width: double.infinity,
                                        ),
                                      ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 45,
                            width: double.infinity,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              onPressed: () {
                                setState(() {
                                  if ((fileType[0] == 'video')) {
                                    _controller.pause();
                                  } else if (fileType[0] == 'audio') {
                                    audioPlayer.stop();
                                  }
                                  fileTypes.removeAt(index);
                                  filesValues.remove(k);

                                  Navigator.pop(context);
                                });
                              },
                              color: Colors.white,
                              textColor: Colors.red,
                              child: Text(
                                (fileType[0] == 'video')
                                    ? 'احذف الفيديو'
                                    : (fileType[0] == 'audio')
                                        ? 'احذف الملف الصوتي'
                                        : 'احذف الصورة',
                                style: TextStyle(fontFamily: 'ar'),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 45,
                            width: double.infinity,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              onPressed: () {
                                if ((fileType[0] == 'video')) {
                                  _controller.pause();
                                } else if (fileType[0] == 'audio') {
                                  audioPlayer.stop();
                                }
                                Navigator.pop(context);
                              },
                              color: Colors.white,
                              textColor: CustomColor.primaryColor,
                              child: Text(
                                'الغاء',
                                style: TextStyle(fontFamily: 'ar'),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                });
          },
          child: Container(
              height: 55,
              width: 55,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: (fileType[0] == 'video' || fileType[0] == 'audio')
                    ? Image.asset('assets/playIcon.png')
                    : Image.file(
                        File(
                          v.toString(),
                        ),
                        fit: BoxFit.fill,
                      ),
              )),
        ));
      });
    return images;
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();

    return MaterialApp(
      title: 'location picker',
      supportedLocales: const <Locale>[
        Locale('en', ''),
        Locale('ar', ''),
      ],
      home: WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            leading: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(
                  Icons.arrow_back_ios,
                  color: CustomColor.primaryColor,
                  size: 20,
                )),
            title: Text(
              'طلب الخدمة',
              style: TextStyle(
                  fontFamily: 'ar',
                  fontSize: 15,
                  color: CustomColor.primaryColor),
            ),
            centerTitle: true,
            actions: <Widget>[
              InkWell(
                child: Container(
                  child: Image.asset(
                    "assets/menu.png",
                    width: 25,
                    height: 18.3,
                  ),
                ),
                onTap: () {
                  _scaffoldKey.currentState.openEndDrawer();
                },
              ),
              SizedBox(
                width: 15,
              )
            ],
          ),
          backgroundColor: Colors.white,
          key: _scaffoldKey,
          endDrawer: Drawer(child: Menu()),
          resizeToAvoidBottomPadding: false,
          resizeToAvoidBottomInset: false,
          body: Builder(builder: (context) {
            return Padding(
              padding: const EdgeInsets.only(left: 15, right: 15, top: 0),
              child: ListView(
                padding: EdgeInsets.all(0),
                children: <Widget>[
                  Form(
                    key: _formKey,
                    child: Column(crossAxisAlignment: CrossAxisAlignment.end,
//                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  "اضف صوره او فيديو او صوت للمشكلة",
                                  textDirection: TextDirection.rtl,
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: "ar",
                                      color: Color.fromRGBO(58, 95, 126, 1)),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 20, bottom: 20),
                                  child: Wrap(
                                    alignment: WrapAlignment.start,
                                    spacing: 15,
                                    runSpacing: 15,
                                    textDirection: TextDirection.rtl,
                                    children: <Widget>[
                                      InkWell(
                                        onTap: () async {
                                          filesPaths = await FilePicker
                                              .getMultiFilePath();
                                          setState(() {
                                            if (filesValues == null) {
                                              filesValues = filesPaths;
                                            } else {
                                              filesValues.addAll(filesPaths);
                                            }
                                          });
                                          print(filesValues);
                                        },
                                        child: Container(
                                          padding: EdgeInsets.all(8),
                                          decoration: BoxDecoration(
                                              color: CustomColor.primaryColor,
                                              borderRadius:
                                                  BorderRadius.circular(8)),
                                          child: DottedBorder(
                                            color: Colors.white,
                                            strokeWidth: 1,
                                            child: Center(
                                              child: Text(
                                                '+',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 20),
                                              ),
                                            ),
                                          ),
                                          width: 55,
                                          height: 55,
                                        ),
                                      ),
                                      //     ...getMyImages(),
                                      ...getGridImages()
                                    ],
                                  ),
                                ),
                                Image.asset("assets/dots.png"),
                                SizedBox(
                                  height: 20,
                                ),
                                Text(
                                  "وصف المشكلة",
                                  textDirection: TextDirection.rtl,
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: "ar",
                                      color: Color.fromRGBO(58, 95, 126, 1)),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Container(
                                  height: 120,
                                  decoration: new BoxDecoration(
                                      //  color: Color.fromRGBO(107, 195, 174, 1),
                                      borderRadius: new BorderRadius.all(
                                        const Radius.circular(8.0),
                                      ),
                                      border: Border.all(
                                          color: Color.fromRGBO(
                                              233, 233, 233, 1))),
                                  child: TextField(
                                    maxLines: 3,
                                    controller: problemController,
                                    textDirection: TextDirection.rtl,
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontFamily: 'rb',
                                        color:
                                            Color.fromRGBO(109, 109, 109, 1)),
                                    decoration: new InputDecoration(
                                        enabledBorder: const OutlineInputBorder(
                                          borderSide: const BorderSide(
                                              color: Colors.white, width: 0.0),
                                        ),
                                        focusedBorder: const OutlineInputBorder(
                                          borderSide: const BorderSide(
                                              color: Colors.white, width: 0.0),
                                        ),
                                        hintText: 'المشكلة',
                                        hintStyle: TextStyle(
                                            fontFamily: "ar",
                                            fontSize: 12,
                                            color: Color.fromRGBO(
                                                113, 145, 175, 0.5))),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Image.asset("assets/dots.png"),
                                SizedBox(
                                  height: 20,
                                ),
                                Text(
                                  "طرق الدفع والتكلفة",
                                  textDirection: TextDirection.rtl,
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: "ar",
                                      color: Color.fromRGBO(58, 95, 126, 1)),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Container(
                                  height: 100,
                                  width: MediaQuery.of(context).size.width,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Text(
                                            "التكلفة",
                                            textAlign: TextAlign.right,
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontFamily: 'rb',
                                                color: Color.fromRGBO(
                                                    109, 109, 109, 1)),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Container(
                                            width: 103,
                                            height: 55,
                                            decoration: new BoxDecoration(
                                                border: Border.all(
                                                    color: Color.fromRGBO(
                                                        58, 95, 126, 1)),
                                                color: Colors.white,
                                                borderRadius:
                                                    new BorderRadius.all(
                                                        Radius.circular(50.0))),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Text(
                                                  widget.price.toString() ?? "",
                                                  style: TextStyle(
                                                      color: Color.fromRGBO(
                                                          48, 83, 121, 1)),
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Image.asset(
                                                  "assets/coins.png",
                                                  width: 28.6,
                                                  height: 20,
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                      SizedBox(
                                        width: 25,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Text(
                                            "كود الخصم",
                                            textAlign: TextAlign.right,
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontFamily: 'rb',
                                                color: Color.fromRGBO(
                                                    109, 109, 109, 1)),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          InkWell(
                                            child: Container(
                                              width: 103,
                                              height: 55,
                                              decoration: new BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      58, 95, 126, 1),
                                                  borderRadius: new BorderRadius
                                                          .all(
                                                      Radius.circular(50.0))),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    "اضف",
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontFamily: "ar"),
                                                  ),
                                                  SizedBox(
                                                    width: 10,
                                                  ),
                                                  Image.asset(
                                                    "assets/tag.png",
                                                    width: 20,
                                                  )
                                                ],
                                              ),
                                            ),
                                            onTap: () {
                                              showModalBottomSheet(
                                                isScrollControlled: true,
                                                isDismissible: false,
                                                useRootNavigator: true,
                                                context: context,
                                                builder: (context) {
                                                  return AnimatedPadding(
                                                    padding:
                                                        MediaQuery.of(context)
                                                            .viewInsets,
                                                    duration: const Duration(
                                                        milliseconds: 100),
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                          color: Colors.white),
                                                      padding: EdgeInsets.only(
                                                          top: 20),
                                                      width: double.infinity,
                                                      child:
                                                          SingleChildScrollView(
                                                        child: Column(
                                                          mainAxisSize:
                                                              MainAxisSize.min,
                                                          children: <Widget>[
                                                            Row(
                                                              textDirection:
                                                                  TextDirection
                                                                      .rtl,
                                                              children: <
                                                                  Widget>[
                                                                Container(
                                                                    margin: EdgeInsets.only(
                                                                        right:
                                                                            15),
                                                                    child:
                                                                        InkWell(
                                                                      child:
                                                                          Icon(
                                                                        Icons
                                                                            .close,
                                                                        size:
                                                                            25,
                                                                        color: CustomColor
                                                                            .primaryColor,
                                                                      ),
                                                                      onTap:
                                                                          () {
                                                                        Navigator.pop(
                                                                            context);
                                                                      },
                                                                    ))
                                                              ],
                                                            ),
                                                            SizedBox(
                                                              height: 20,
                                                            ),
                                                            Image.asset(
                                                              "assets/copon.png",
                                                              width: 60,
                                                            ),
                                                            SizedBox(
                                                              height: 30,
                                                            ),
                                                            Text(
                                                              'كود الخصم',
                                                              style: TextStyle(
                                                                  fontFamily:
                                                                      "ar",
                                                                  fontSize: 18,
                                                                  color: Color
                                                                      .fromRGBO(
                                                                          58,
                                                                          95,
                                                                          126,
                                                                          1)),
                                                            ),
                                                            Text(
                                                              'ادخل كود الخصم حتي تحصل علي',
                                                              style: TextStyle(
                                                                  fontFamily:
                                                                      "rb",
                                                                  fontSize: 16,
                                                                  color: Colors
                                                                      .grey),
                                                            ),
                                                            Text(
                                                              'خصم علي الطلب',
                                                              style: TextStyle(
                                                                  fontFamily:
                                                                      "rb",
                                                                  fontSize: 16,
                                                                  color: Colors
                                                                      .grey),
                                                            ),
                                                            SizedBox(
                                                              height: 20,
                                                            ),
                                                            Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      right:
                                                                          20),
                                                              child: Row(
                                                                textDirection:
                                                                    TextDirection
                                                                        .rtl,
                                                                children: <
                                                                    Widget>[
                                                                  Text(
                                                                    'ادخل الكود',
                                                                    style: TextStyle(
                                                                        fontFamily:
                                                                            "ar",
                                                                        fontSize:
                                                                            16,
                                                                        color: Color.fromRGBO(
                                                                            58,
                                                                            95,
                                                                            126,
                                                                            1)),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            Container(
                                                              margin: EdgeInsets
                                                                  .all(20),
                                                              padding: EdgeInsets
                                                                  .only(
                                                                      left: 20,
                                                                      right:
                                                                          20),
                                                              decoration:
                                                                  new BoxDecoration(
                                                                color: Colors
                                                                    .white,
                                                                border: Border.all(
                                                                    color: Colors
                                                                            .grey[
                                                                        300],
                                                                    width: 1),
                                                                borderRadius:
                                                                    new BorderRadius
                                                                        .all(
                                                                  const Radius
                                                                          .circular(
                                                                      5.0),
                                                                ),
                                                              ),
                                                              child: Center(
                                                                child:
                                                                    PinCodeTextField(
                                                                  length: 5,
                                                                  obsecureText:
                                                                      false,
                                                                  inactiveColor:
                                                                      Colors.grey[
                                                                          300],
                                                                  animationType:
                                                                      AnimationType
                                                                          .fade,
                                                                  shape: PinCodeFieldShape
                                                                      .underline,
                                                                  animationDuration:
                                                                      Duration(
                                                                          milliseconds:
                                                                              300),
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  fieldHeight:
                                                                      40,
                                                                  fieldWidth:
                                                                      40,
                                                                  onCompleted:
                                                                      (v) {
                                                                    pinCodeValue =
                                                                        v;
                                                                  },
                                                                  onChanged: (String
                                                                      value) {},
                                                                ),
                                                              ),
                                                            ),
                                                            SizedBox(
                                                              height: 20,
                                                            ),
                                                            Container(
                                                              margin: EdgeInsets
                                                                  .fromLTRB(
                                                                      20,
                                                                      0,
                                                                      20,
                                                                      20),
                                                              width:
                                                                  MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width,
                                                              height: 50,
                                                              child:
                                                                  RaisedButton(
                                                                shape: RoundedRectangleBorder(
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            8)),
                                                                color: Color
                                                                    .fromRGBO(
                                                                        58,
                                                                        95,
                                                                        126,
                                                                        1),
                                                                onPressed:
                                                                    () async {
                                                                  checkpromoCode(
                                                                          pinCodeValue)
                                                                      .then(
                                                                          (v) {
                                                                    if (v ==
                                                                        'availabe') {
                                                                      finalCodeVal =
                                                                          pinCodeValue;
                                                                      Navigator.pop(
                                                                          context);
                                                                      /*setState(() {
                                                                          showBottomSheet =
                                                                              false;
                                                                        });*/
                                                                    } else if (v ==
                                                                        'expired') {
                                                                      print(
                                                                          'CodeExpired');

                                                                      Fluttertoast.showToast(
                                                                          msg:
                                                                              "صلاحية الكود منتهية",
                                                                          toastLength: Toast
                                                                              .LENGTH_SHORT,
                                                                          gravity: ToastGravity
                                                                              .CENTER,
                                                                          timeInSecForIos:
                                                                              1,
                                                                          backgroundColor: CustomColor
                                                                              .primaryColor,
                                                                          textColor: Colors
                                                                              .white,
                                                                          fontSize:
                                                                              16.0);
                                                                      //   Navigator.pop(context);
                                                                    } else {
                                                                      pinCodeValue =
                                                                          '';
                                                                      Fluttertoast.showToast(
                                                                          msg:
                                                                              "من فضلك ادخل كود خصم صحيح",
                                                                          toastLength: Toast
                                                                              .LENGTH_SHORT,
                                                                          gravity: ToastGravity
                                                                              .CENTER,
                                                                          timeInSecForIos:
                                                                              1,
                                                                          backgroundColor: CustomColor
                                                                              .primaryColor,
                                                                          textColor: Colors
                                                                              .white,
                                                                          fontSize:
                                                                              16.0);
                                                                    }
                                                                  });
                                                                },
                                                                child: Text(
                                                                  "اضافة",
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .white,
                                                                      fontFamily:
                                                                          "ar"),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  );
                                                },
                                              );
                                            },
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 41,
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 50,
                                  child: RaisedButton(
                                    color: Color.fromRGBO(107, 195, 174, 1),
                                    shape: RoundedRectangleBorder(
                                        borderRadius: new BorderRadius.all(
                                      const Radius.circular(8.0),
                                    )),
                                    onPressed: () async {
                                      showDialog(
                                          context: context,
                                          barrierDismissible: false,
                                          builder: (context) => AlertDialog(
                                                content: Row(
                                                  textDirection:
                                                      TextDirection.rtl,
                                                  children: <Widget>[
                                                    CircularProgressIndicator(),
                                                    SizedBox(
                                                      width: 25,
                                                    ),
                                                    Text('جاري ارسال الطلب')
                                                  ],
                                                ),
                                              ));
                                      await _makePostRequest(servID, subID)
                                          .whenComplete(() {
                                        //      Navigator.pop(this.context);
                                        print('DoneCompleterd');
                                        Navigator.of(context,
                                                rootNavigator: true)
                                            .pop('dialog');
                                      }).then((v) {
                                        print('ResponseThen:$v');

                                        Navigator.pushReplacement(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    OrderConnfirm()));
                                      }).catchError((e) {
                                        print('ErrorCatch:$e');
                                        Fluttertoast.showToast(
                                            msg: e.toString(),
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.CENTER,
                                            timeInSecForIos: 1,
                                            backgroundColor:
                                                CustomColor.primaryColor,
                                            textColor: Colors.white,
                                            fontSize: 16.0);
                                      });

                                      /*      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  OrderConnfirm()));
                                   */
                                    },
                                    child: Text(
                                      "ارسال الطلب",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "ar"),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 40,
                                )
                              ],
                            ),
                          ),
                        ]),
                  )
                ],
              ),
            );
          }),
        ),
      ),
    );
  }

  List<Widget> getMyImages() {
    List<Widget> images = [];
    images.add(Container(
      width: 50,
      height: 50,
      color: Colors.red,
    ));
    images.add(Container(
      width: 50,
      height: 50,
      color: Colors.red,
    ));
    images.add(Container(
      width: 50,
      height: 50,
      color: Colors.red,
    ));
    images.add(Container(
      width: 50,
      height: 50,
      color: Colors.red,
    ));
    images.add(Container(
      width: 50,
      height: 50,
      color: Colors.red,
    ));

    return images;
  }
}
