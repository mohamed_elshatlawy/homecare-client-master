import 'dart:convert' as prefix0;
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:homecare/color/myColor.dart';
//import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:homecare/home.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:homecare/menu.dart';
import 'package:homecare/order/attach.dart';
import 'package:homecare/providers/user.dart';
import 'package:intl/intl.dart' as time;
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:place_picker/place_picker.dart';

class OrderMap extends StatefulWidget {
  final String title;
  final String serviceID;
  final String description;
  final LocationResult result;
  final String price;

  OrderMap(
      {this.title,
      this.serviceID,
      this.description,
      this.price,
      Key key,
      this.result})
      : super(key: key);

  @override
  _OrderMapState createState() => _OrderMapState(result);
}

class _OrderMapState extends State<OrderMap> {
  LocationResult result;
  _OrderMapState(this.result) {
    _pickedLocation = result;
  }

  String selectDate;
  String selectTime;
  String body;
  String service;
  String subService;
  bool load = false;

  LocationResult _pickedLocation;

  _makePostRequest() async {
    final user = Provider.of<UserModel>(context);
    String url = 'https://delivery.digitalharbor.me//api/request?locale=ar';
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: "application/json",
      HttpHeaders.authorizationHeader: "Bearer " + user.userToken,
    };
    String json = '{"service_id": "' +
        widget.serviceID +
        '", "district_id": "1", "address":"' +
        _pickedLocation.formattedAddress.toString() +
        '", "longitude":"' +
        _pickedLocation.latLng.longitude.toString() +
        '" , "latitude":"' +
        _pickedLocation.latLng.latitude.toString() +
        '" , "time":"' +
        selectTime +
        '" , "date":"' +
        selectDate +
        '"}';
    http.Response response = await http.post(url, headers: headers, body: json);
    //int statusCode = response.statusCode;

    setState(() {
      body = response.body;
      print('Body:$body');
      Map valueMap = prefix0.json.decode(body);
      service = valueMap['service_request_id'].toString();
      subService = valueMap['subscription_id'].toString();
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  String shift = "AM";
  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();

    return MaterialApp(
      title: 'location picker',
      supportedLocales: const <Locale>[
        Locale('en', ''),
        Locale('ar', ''),
      ],
      home: Scaffold(
        backgroundColor: Colors.white,
        key: _scaffoldKey,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            leading: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(
                  Icons.arrow_back_ios,
                  color: CustomColor.primaryColor,
                  size: 20,
                )),
            title: Text(
              'طلب الخدمة',
              style: TextStyle(
                  fontFamily: 'ar',
                  fontSize: 15,
                  color: CustomColor.primaryColor),
            ),
            centerTitle: true,
            actions: <Widget>[
              InkWell(
                child: Container(
                  child: Image.asset(
                    "assets/menu.png",
                    width: 25,
                    height: 18.3,
                  ),
                ),
                onTap: () {
                  _scaffoldKey.currentState.openEndDrawer();
                },
              ),
              SizedBox(
                width: 15,
              )
            ],
          ),
        ),
        endDrawer: Drawer(child: Menu()),
        body: Builder(builder: (context) {
          return Padding(
            padding: const EdgeInsets.only(left: 10, right: 15, top: 0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Text(
                    "عنوان طلب الصيانة",
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: "ar",
                        color: Color.fromRGBO(58, 95, 126, 1)),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 10, left: 20, right: 0),
                    child: Row(
                      textDirection: TextDirection.rtl,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.location_on,
                          size: 17,
                          color: Color.fromRGBO(48, 83, 121, 1),
                        ),
                        Flexible(
                          child: Text(
                            _pickedLocation.toString() == "null"
                                ? ""
                                : _pickedLocation.formattedAddress,
                            textDirection: TextDirection.rtl,
                            style: TextStyle(fontFamily: "ar", fontSize: 11),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                 Container(
                    child: Row(
                      textDirection: TextDirection.rtl,
                      children: <Widget>[
                        InkWell(
                          onTap: () async {
                          // LocationResult result = await LocationPicker.pickLocation(
                        //   context,
                        //   "AIzaSyArqxjb1ObPE7H-KcaCr1RExwCLnajHDcU",
                        // );

                        result =
                            await Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => PlacePicker(
                                      "AIzaSyArqxjb1ObPE7H-KcaCr1RExwCLnajHDcU",
                                    )));

                        setState(() => _pickedLocation = result);
                        // Handle the result in your way
                        print(result.locality);   },
                          child: Text(
                            'تغيير العنوان',
                            style: TextStyle(
                              fontFamily: "rb",
                              color: Color.fromRGBO(53, 172, 144, 1),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                    SizedBox(
                    height: 10,
                  ),
                  Image.asset("assets/dots.png"),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "معلومات الطلب",
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                        fontSize: 18,
                        fontFamily: "ar",
                        color: Color.fromRGBO(58, 95, 126, 1)),
                  ),
                  SizedBox(
                    height: 14,
                  ),
                  Text(
                    "نوع الخدمة",
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: "rb",
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(4, 83, 152, 1)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10, left: 20, right: 0),
                    child: Text(
                      widget.title ?? "",
                      textDirection: TextDirection.rtl,
                      style: TextStyle(
                        fontSize: 16,
                        fontFamily: "rb",
                        color: Color.fromRGBO(81, 81, 81, 1),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20, right: 0),
                    child: Text(
                      widget.description ?? "",
                      textDirection: TextDirection.rtl,
                      style: TextStyle(
                        fontSize: 16,
                        fontFamily: "rb",
                        color: Color.fromRGBO(81, 81, 81, 1),
                      ),
                    ),
                  ),
            
                    SizedBox(height: 20,),
                  Container(
                    child: Row(
                      textDirection: TextDirection.rtl,
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Home()));
                          },
                          child: Text(
                            'تغيير نوع الخدمة',
                            style: TextStyle(
                              fontFamily: "rb",
                              color: Color.fromRGBO(53, 172, 144, 1),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                 
                    SizedBox(height: 10,),
                  Image.asset("assets/dots.png"),
                  Text(
                    "الوقت و التاريخ",
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: "ar",
                        color: Color.fromRGBO(58, 95, 126, 1)),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 50,
                    decoration: new BoxDecoration(
                      color: Color.fromRGBO(48, 83, 121, 1),
                      borderRadius: new BorderRadius.all(
                        const Radius.circular(8.0),
                      ),
                    ),
                    child: FlatButton(
                        onPressed: () {
                          DatePicker.showTimePicker(context,
                              showTitleActions: true,
                              onChanged: (date) {}, onConfirm: (date) {
                            print('weeeee');
                            print(date.hour);
                            if (date.hour > 12) {
                              shift = 'PM';
                            } else {
                              shift = 'AM';
                            }
                            print(shift);
                            setState(() {
                              selectTime = time.DateFormat('hh:mm')
                                  .format(date)
                                  .toString();
                              print(selectTime);

                              //   selectTime='$selectTime $shift';
                            });
                          },
                              currentTime: DateTime.now(),
                              locale: LocaleType.en);
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              selectTime == null ? "" : '$selectTime $shift',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16),
                            ),
                            Icon(
                              Icons.add,
                              color: Colors.white,
                            )
                          ],
                        )),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 50,
                    decoration: new BoxDecoration(
                      color: Color.fromRGBO(48, 83, 121, 1),
                      borderRadius: new BorderRadius.all(
                        const Radius.circular(8.0),
                      ),
                    ),
                    child: FlatButton(
                        onPressed: () {
                          DatePicker.showDatePicker(context,
                              showTitleActions: true,
                              minTime: DateTime.now(), onChanged: (date) {
                            print('change $date');
                          }, onConfirm: (date) {
                            setState(() {
                              selectDate = time.DateFormat('yyyy-MM-dd')
                                  .format(date)
                                  .toString();
                            });
                          },
                              currentTime: DateTime.now(),
                              locale: LocaleType.en);
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              selectDate == null ? "" : selectDate,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16),
                            ),
                            Icon(
                              Icons.add,
                              color: Colors.white,
                            )
                          ],
                        )),
                  ),
                  SizedBox(
                    height: 45,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 48,
                    decoration: new BoxDecoration(
                      color: Color.fromRGBO(107, 195, 174, 1),
                      borderRadius: new BorderRadius.all(
                        const Radius.circular(8.0),
                      ),
                    ),
                    child: FlatButton(
                      onPressed: () async {
                        if ("_pickedLocation" == null ||
                            selectTime == null ||
                            selectDate == null) {
                          showDialog<void>(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                content: const Text(
                                  'نرجو اختيار العنوان والتاريخ والوقت',
                                  style: TextStyle(fontFamily: "ar"),
                                ),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text(
                                      'اعد التجربه',
                                      style: TextStyle(fontFamily: "ar"),
                                    ),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ],
                              );
                            },
                          );
                        } else {
                          if (load == false) {
                            load = true;
                            await _makePostRequest();

                            print('$service ${widget.price}');

                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => OrderAttach(
                                          id: service,
                                          subID: subService,
                                          price: widget.price,
                                        )));
                          }
                        }
                      },
                      child: Text(
                        "التالى",
                        style: TextStyle(color: Colors.white, fontFamily: "ar"),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  )
                ],
              ),
            ),
          );
        }),
      ),
    );
  }
}



