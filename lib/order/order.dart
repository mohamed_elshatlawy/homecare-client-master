import 'dart:convert';
import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:homecare/backEnd/service.dart';
import 'package:homecare/color/myColor.dart';
import 'package:homecare/contact.dart';
import 'package:homecare/dna/error.dart';
import 'package:homecare/menu.dart';
import 'package:homecare/order/complain.dart';
import 'package:homecare/providers/user.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:intl/intl.dart' as time;

class Order extends StatefulWidget {
  final String orderId;
  final String token;

  Order({this.orderId, this.token, Key key}) : super(key: key);

  @override
  _OrderState createState() => _OrderState(token);
}

TextEditingController problemController = new TextEditingController();

class _OrderState extends State<Order> {
  final String token;
  _OrderState(this.token);
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      getData();
    });
  }

  var data;
  Future<String> getData() async {
    final user = Provider.of<UserModel>(context);

    var response = await http.get(
        Uri.encodeFull("http://delivery.digitalharbor.me//api/request/" +
            widget.orderId +
            "?locale=ar"),
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer " + user.userToken,
        });
    setState(() {
      data = json.decode(response.body);
      print(response.body);
    });

    return "Success";
  }

  List<Widget> getLine() {
    List<Widget> line = [];
    List myData = data['timeline'];
    print('Lengh:${myData.length}');
    for (int index = 0; index < myData.length; index++) {
      line.add(Padding(
        padding: const EdgeInsets.only(left: 15, right: 15, top: 0, bottom: 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                    width: 80,
                    child: Text(
                      data['timeline'][index]['action'],
                      style: TextStyle(
                          fontSize: 14,
                          color: (index == data['timeline'].length - 1)
                              ? CustomColor.primaryColor
                              : Colors.black87),
                    )),
                Container(
                    padding: EdgeInsets.all(3),
                    width: 15.0,
                    height: 15.0,
                    decoration: new BoxDecoration(
                      border: Border.all(
                          color: (index == data['timeline'].length - 1)
                              ? CustomColor.secondartColor
                              : Colors.grey,
                          width: 1),
                      shape: BoxShape.circle,
                    ),
                    child: (index == data['timeline'].length - 1)
                        ? Container(
                            decoration: BoxDecoration(
                                color: CustomColor.secondartColor,
                                shape: BoxShape.circle),
                          )
                        : Container()),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      time.DateFormat('yyyy-MM-dd')
                          .format(DateTime.parse(
                              data['timeline'][index]['created_at']))
                          .toString(),
                      style: TextStyle(
                          color: (index == data['timeline'].length - 1)
                              ? CustomColor.primaryColor
                              : Colors.grey,
                          fontSize: 14),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      time.DateFormat('hh-mm a')
                          .format(DateTime.parse(
                              data['timeline'][index]['created_at']))
                          .toString(),
                      style: TextStyle(
                          color: (index == data['timeline'].length - 1)
                              ? CustomColor.primaryColor
                              : Colors.grey,
                          fontSize: 14,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ],
            ),
            (index == data['timeline'].length - 1)
                ? Container()
                : Container(
                    margin: EdgeInsets.only(left: 10),
                    height: 40,
                    width: 1,
                    color: Colors.grey,
                  )
          ],
        ),
      ));
    }
    return line;
  }

  Widget getTimeLineList() {
    /* if (data == null || data['timeline'].length < 1) {
      return Container(
          height: 0,
          width: MediaQuery.of(context).size.width,
          child: Container());
    }*/
    if (data == null) {
      return Container(
          height: 0,
          width: MediaQuery.of(context).size.width,
          child: Container());
    } else if (data['type'] == 'pricing' &&
        data['status'] == 'created' &&
        data['price'] == 0) {
      return Container(
          width: MediaQuery.of(context).size.width,
          child: Container(
            child: Center(child: Text('الطلب تحت المراجعة برجاء الانتظار',style: TextStyle(
              fontFamily: 'rb'
            ),)),
          ));
    } else if(data['type'] == 'pricing' &&
        data['status'] == 'created' &&
        data['price'] > 0) {
      return Container(
        child: (confirmedRequest == false)
            ? Row(
                textDirection: TextDirection.rtl,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'تم تحديد تكلفة الطلب : ${data['price']}',
                    style: TextStyle(
                      fontFamily: 'rb'
                    ),
                    textAlign: TextAlign.right,
                  ),
                  RaisedButton(
                    onPressed: () async {
                      showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (context) => AlertDialog(
                                content: Row(
                                  textDirection: TextDirection.rtl,
                                  children: <Widget>[
                                    CircularProgressIndicator(),
                                    SizedBox(
                                      width: 25,
                                    ),
                                    Text('جاري تأكيد الطلب ')
                                  ],
                                ),
                              ));
                      await confirmOrderPrice(data['id'], token).then((v) {
                        if (v is String) {
                          setState(() {
                            confirmedRequest = true;
                          });
                        }
                      }).whenComplete(() {
                      Navigator.of(context, rootNavigator: true).pop('dialog');
                      }).catchError((e) {
                        print('ErrorReq:$e');
                      });
                    },
                    color: CustomColor.secondartColor,
                    textColor: Colors.white,
                    child: Text('تأكيد',style: TextStyle(fontFamily: 'rb'),),
                  )
                ],
              )
            : Text('تم ارسال الطلب بنجاح',style: TextStyle(
              fontFamily: 'rb'
            ),),
      );
    } else if (data['timeline'].length > 0) {
      return Column(
        children: <Widget>[...getLine()],
      );
    }
  }

  bool confirmedRequest = false;
  @override
  Widget build(BuildContext context) {
    ErrorWidget.builder = (FlutterErrorDetails errorDetails) {
      return getErrorWidget(context, errorDetails);
    };

    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();

    return MaterialApp(
        title: 'Homecare',
        supportedLocales: const <Locale>[
          Locale('en', ''),
          Locale('ar', ''),
        ],
        home: Scaffold(
            key: _scaffoldKey,
            endDrawer: Drawer(child: Menu()),
            body: SingleChildScrollView(
              child: Column(children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 30, bottom: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          margin: EdgeInsets.only(left: 10),
                          child: Icon(
                            Icons.chevron_left,
                            size: 24,
                            color: Color.fromRGBO(48, 83, 121, 1),
                          ),
                        ),
                      ),
                      Text(
                        "قائمة الطلبات",
                        style: TextStyle(
                            fontSize: 20,
                            fontFamily: "ar",
                            color: Color.fromRGBO(58, 95, 126, 1)),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 10),
                        child: InkWell(
                          child: Image.asset(
                            "assets/menu.png",
                            width: 24,
                          ),
                          onTap: () {
                            _scaffoldKey.currentState.openEndDrawer();
                          },
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  child: Card(
                    child: Padding(
                      padding:
                          const EdgeInsets.only(left: 20, right: 20, top: 20),
                      child: Column(
                        children: <Widget>[
                          getTimeLineList(),
                          SizedBox(
                            height: 37,
                          ),
                          Container(
                            height: 1,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Row(
                            textDirection: TextDirection.rtl,
                            children: <Widget>[
                              Icon(
                                Icons.location_on,
                                color: CustomColor.secondartColor,
                                size: 20,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                "العنوان",
                                style: TextStyle(
                                    fontSize: 14,
                                    fontFamily: "ar",
                                    color: Color.fromRGBO(58, 95, 126, 1)),
                              ),
                            ],
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            data['address'].toString(),
                            textAlign: TextAlign.right,
                            style: TextStyle(
                                color: Colors.grey, fontSize: 14, height: 1.5),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Container(
                            height: 1,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Row(
                            textDirection: TextDirection.rtl,
                            children: <Widget>[
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Text(
                                      "التاريخ",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontFamily: "ar",
                                          color:
                                              Color.fromRGBO(58, 95, 126, 1)),
                                    ),
                                    Text(
                                      data['date'].toString(),
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Text(
                                      "الوقت",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontFamily: "ar",
                                          color:
                                              Color.fromRGBO(58, 95, 126, 1)),
                                    ),
                                    Text(
                                      data['time'].toString(),
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Container(
                            height: 1,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Row(
                            textDirection: TextDirection.rtl,
                            children: <Widget>[
                              Column(
                                children: <Widget>[
                                  Text(
                                    "الفني",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontFamily: "ar",
                                        color: Color.fromRGBO(58, 95, 126, 1)),
                                  ),
                                  Text(data['technician_id'].toString() ==
                                          "null"
                                      ? " "
                                      : data['technician']['name'].toString()),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Container(
                            height: 1,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Row(
                            textDirection: TextDirection.rtl,
                            children: <Widget>[
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Text(
                                      "طريقة الدفع",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontFamily: "ar",
                                          color: CustomColor.primaryColor),
                                    ),
                                    Text(
                                      data['payment_method']
                                          .toString()
                                          .toUpperCase(),
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Text(
                                      "التكلفة",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontFamily: "ar",
                                          color: CustomColor.primaryColor),
                                    ),
                                    Text(
                                      data['total'].toString() + " EGP",
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Container(
                            height: 1,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(left: 20, right: 20),
                                  width: MediaQuery.of(context).size.width,
                                  height: 50,
                                  child: RaisedButton(
                                    color: Color.fromRGBO(107, 195, 174, 1),
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => Contact()));
                                    },
                                    child: Text(
                                      "اتصل بنا",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "rb",
                                          fontSize: 16),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 20, right: 20),
                                  width: MediaQuery.of(context).size.width,
                                  height: 50,
                                  child: RaisedButton(
                                    color: Colors.red,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => Complain(
                                                    orderID:
                                                        data['id'].toString(),
                                                  )));
                                    },
                                    child: Text(
                                      "الابلاغ عن مشكلة",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                          fontFamily: "rb"),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ]),
            )));
  }
}
