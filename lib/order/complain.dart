import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:homecare/menu.dart';
import 'package:homecare/providers/user.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

class Complain extends StatefulWidget {
  final String orderID;
  Complain({this.orderID, Key key}) : super(key: key);
  _ComplainState createState() => _ComplainState();
}

class _ComplainState extends State<Complain> {
  @override
  void initState() {
    super.initState();
  }

  static final _formKey = new GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();

    TextEditingController problemController = new TextEditingController();

    _makePostRequest() async {
      final user = Provider.of<UserModel>(context);
      String url =
          'http://delivery.digitalharbor.me//api/request/1/complaign?locale=ar';
      Map<String, String> headers = {
        HttpHeaders.contentTypeHeader: "application/json",
        HttpHeaders.authorizationHeader: "Bearer " + user.userToken,
      };
      String json = '{"description": "' + problemController.text + '"}';
      http.Response response =
          await http.post(url, headers: headers, body: json);

      // setState(() {
      //   body = response.body;
      // });
      print(response.body);
    }

    return Consumer<UserModel>(builder: (context, user, _) {
      return Scaffold(
        key: _scaffoldKey,
        endDrawer: Drawer(child: Menu()),
        body: Container(
          child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 30, left: 10, right: 10, bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        InkWell(
                          onTap: (){
                            Navigator.pop(context);
                          },
                                                  child: Icon(
                            Icons.chevron_left,
                             size: 24,
                            color: Color.fromRGBO(48, 83, 121, 1),
                          ),
                        ),
                        Text(
                          "الابلاغ عن مشكلة",
                          style: TextStyle(
                              fontSize: 20,
                              fontFamily: "ar",
                              color: Color.fromRGBO(58, 95, 126, 1)),
                        ),
                        InkWell(
                          child: Image.asset(
                            "assets/menu.png",
                            width: 24,
                          ),
                          onTap: () {
                            _scaffoldKey.currentState.openEndDrawer();
                          },
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                 Container(
                   margin: EdgeInsets.only(right: 20,left: 20),
                   child: Column(
                     children: <Widget>[
                        Row(
                      textDirection: TextDirection.rtl,
                      children: <Widget>[
                        Text(
                          "وصف المشكلة",
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "ar",
                              color: Color.fromRGBO(58, 95, 126, 1)),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Form(
                      key: _formKey,
                      child: TextField(
                        controller: problemController,
                        textDirection: TextDirection.rtl,
                        textAlign: TextAlign.right,
                        decoration: new InputDecoration(
                          enabledBorder: const OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: Colors.grey, width: 0.0),
                          ),
                          focusedBorder: const OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: Colors.grey, width: 0.0),
                          ),
                          hintText: 'نص المشكلة',
                          hintStyle: TextStyle(
                            fontFamily: 'rb'
                          )
                        ),
                        maxLines: 5,
                      ),
                    ),
                    SizedBox(
                      height: 50,
                    ),
                     ],
                   ),
                 )
              
                ],
              ),
            
            Align(
              alignment: Alignment.bottomCenter,
              child:  Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(bottom: 20,left: 20,right: 20),
                
                  height: 50,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(6)
                    ),
                    color: Color.fromRGBO(255, 92,80, 1),
                    onPressed: () {
                      _makePostRequest();
                      Navigator.of(context).pop();
                      
                    },
                    child: Text(
                      "الابلاغ عن مشكلة",
                      style: TextStyle(color: Colors.white, fontFamily: "rb",
                      fontSize: 16
                      ),
                    ),
                  ),
                ),
          
               
            )
            ],
          ),
        ),
      );
    });
  }
}
