import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:homecare/home.dart';
import 'package:homecare/menu.dart';
import 'package:homecare/order/order.dart';
import 'package:homecare/providers/user.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class Orders extends StatefulWidget {
  final String type;
  final String userName;
  final String userToken;
  final String userType;
  Orders({this.type, this.userName, this.userToken, this.userType, Key key})
      : super(key: key);

  @override
  _OrdersState createState() => _OrdersState(userToken);
}

TextEditingController problemController = new TextEditingController();

class _OrdersState extends State<Orders> {
  String token;
  _OrdersState(this.token);
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      final user = Provider.of<UserModel>(context);
      if (widget.userName != null) {
        user.userName = widget.userName;
        user.userToken = widget.userToken;
      }
      getData();
    });
  }

  var data;
  Future<String> getData() async {
    final user = Provider.of<UserModel>(context);
    //print(user.userToken);
    var response = await http.get(
        Uri.encodeFull(
            "http://delivery.digitalharbor.me//api/request?locale=ar"),
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer " + user.userToken,
        });
    setState(() {
      data = json.decode(response.body);
    });

    return "Success";
  }

  Widget getScheduledList() {
    if (data == null || data.length < 1) {
      return Container(
        height: MediaQuery.of(context).size.height * 0.6,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }

    return Column(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height * 0.78,
          width: MediaQuery.of(context).size.width,
          child: ListView.builder(
            itemCount: data['scheduled']?.length,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding: const EdgeInsets.only(left: 12, right: 12, top: 10),
                child: getListItem(index, 'scheduled'),
              );
            },
          ),
        ),
      ],
    );
  }

  Widget getHistoryList() {
    if (data == null || data.length < 1) {
      return Container(
        height: MediaQuery.of(context).size.height * 0.78,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    return Column(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height * 0.6,
          width: MediaQuery.of(context).size.width,
          child: ListView.builder(
            itemCount: data['history']?.length,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding: const EdgeInsets.only(left: 8, right: 8, top: 5),
                child: getListItem(index, 'history'),
              );
            },
          ),
        ),
      ],
    );
  }

  Widget getListItem(int i, String type) {
    if (data == null || data.length < 1) return null;

    return InkWell(
      child: Container(
        height: 180,
        width: MediaQuery.of(context).size.width,
        child: Card(
          elevation: 2,
          clipBehavior: Clip.antiAlias,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          color: Color.fromRGBO(242, 248, 255, 1),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width * .91,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                  
                 
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 10, right: 10, top: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text(data[type][i]['service']['title'].toString(),
                                textAlign: TextAlign.right,
                                textDirection: TextDirection.rtl,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                  color: Color.fromRGBO(58, 95, 126, 1),
                                  fontFamily: "ar",
                                )),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Flexible(
                            child: RichText(
                              //overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.right,
                              strutStyle: StrutStyle(fontSize: 12.0),
                              text: TextSpan(
                                style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: "rb",
                                  color: Color.fromRGBO(120, 120, 120, 1),
                                ),
                                text:
                                    data['scheduled'][i]['address'].toString(),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Icon(
                            Icons.location_on,
                            size: 20,
                            color: Color.fromRGBO(48, 83, 121, 1),
                          )
                        ],
                      ),
                    ),
                    Container(
                      decoration: new BoxDecoration(
                          color: Color.fromRGBO(214, 233, 255, 1),
                          borderRadius: new BorderRadius.only(
                              bottomLeft: const Radius.circular(10.0),
                              bottomRight: const Radius.circular(10.0))),
                      height: 50,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 14, right: 14),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(top: 3.0),
                                  child: Text(
                                    data[type][i]['time'].toString(),
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontFamily: "ar",
                                      fontWeight: FontWeight.bold,
                                      color: Color.fromRGBO(100, 100, 100, 1),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 6,
                                ),
                                Icon(
                                  Icons.access_time,
                                  size: 20,
                                  color: Color.fromRGBO(48, 83, 121, 1),
                                )
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(top: 4.0),
                                  child: Text(
                                    data[type][i]['date'].toString(),
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontFamily: "ar",
                                      fontWeight: FontWeight.bold,
                                      color: Color.fromRGBO(48, 83, 121, 1),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 6,
                                ),
                                Icon(
                                  Icons.calendar_today,
                                  size: 20,
                                  color: Color.fromRGBO(48, 83, 121, 1),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      onTap: () {
         final user = Provider.of<UserModel>(context);
        print('id:${data[type][i]['id'].toString()}');
        print('Token:$token');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => Order(
                      orderId: data[type][i]['id'].toString(),
                      token: user.userToken
                    )));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();

    return MaterialApp(
      title: 'HomeCare',
      home: WillPopScope(
        onWillPop: () async {
             return await Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (context) => Home()));
         
        },
              child: DefaultTabController(
          length: 2,
          child: Scaffold(
            
            appBar: AppBar(
                elevation: 0,
                centerTitle: true,
                title: Text(
                  "الطلبات",
                  style: TextStyle(
                    fontSize: 20,
                    fontFamily: "ar",
                    color: Color.fromRGBO(58, 95, 126, 1),
                  ),
                ),
                backgroundColor: Colors.white,
              leading:       InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          margin: EdgeInsets.only(left: 10),
                          child: Icon(
                            Icons.chevron_left,
                            size: 24,
                            color: Color.fromRGBO(48, 83, 121, 1),
                          ),
                        ),
                      ),
                actions: <Widget>[
                  InkWell(
                    child: Container(
                      child: Image.asset(
                        "assets/menu.png",
                        width: 25,
                        height: 18.3,
                      ),
                    ),
                    onTap: () {
                      _scaffoldKey.currentState.openEndDrawer();
                    },
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  
                ],
                bottom: PreferredSize(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          bottom: 8.0, right: 16, left: 16),
                      child: Container(
                          height: 38,
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              border: Border.all(
                                color: Color.fromRGBO(58, 95, 126, 1),
                              )),
                          child: TabBar(
                            indicatorColor: Colors.transparent,
                            unselectedLabelColor:
                                Color.fromRGBO(58, 95, 126, 1),
                            labelColor: Colors.white,
                            labelStyle: TextStyle(color: Colors.white),
                            indicatorSize: TabBarIndicatorSize.tab,
                            indicator: BoxDecoration(
                                borderRadius: BorderRadius.circular(6),
                                color: Color.fromRGBO(58, 95, 126, 1)),
                            tabs: [
                              Container(
                                child: Tab(
                                    //                  icon: Icon(Icons.timelapse),
                                    child: Container(
                                  child: Text(
                                    "الحالي",
                                    style: TextStyle(
                                        fontSize: 12, fontFamily: "ar"),
                                  ),
                                )),
                              ),
                              Container(
                                child: Tab(
                                  //                  icon: Icon(Icons.av_timer),
                                  child: Text(
                                    "السابق",
                                    style: TextStyle(
                                        fontSize: 12, fontFamily: "ar"),
                                  ),
                                ),
                              ),
                            ],
                          )),
                    ),
                    preferredSize:
                        Size(MediaQuery.of(context).size.width, 50))),
            key: _scaffoldKey,
            endDrawer: Drawer(child: Menu()),
            body: Builder(builder: (context) {
              return TabBarView(
                children: <Widget>[
                  getScheduledList(),
                  getHistoryList(),
                ],
              );
            }),
          ),
        ),
      ),
    );
  }
}
