import 'package:flutter/material.dart';
import 'package:homecare/access/intro.dart';
import 'package:homecare/access/login.dart';
import 'package:homecare/providers/user.dart';
import 'package:homecare/search.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  bool _seen = (prefs.getBool('seen') ?? false);
  String _userName = prefs.getString('name');
  String _userToken = prefs.getString('token');

  if (_seen) {
    if (_userName == null) {
      runApp(MultiProvider(providers: [
        ChangeNotifierProvider(builder: (_) => UserModel()),
      ], child: MaterialApp(
        title: 'Homecare',
        home: Login())));
    } else {
      runApp(MultiProvider(
          providers: [
            ChangeNotifierProvider(builder: (_) => UserModel()),
          ],
          child: MaterialApp(
            title: 'Homecare',
              home: Search(
               userName: _userName,
               userToken: _userToken,
          ))));
    }
  } else {
    prefs.setBool('seen', true);
    runApp(MultiProvider(providers: [
      ChangeNotifierProvider(builder: (_) => UserModel()),
    ], child: MaterialApp(
      title: 'Homecare',
      home: Intro())));
  }
}

class Main extends StatefulWidget {
  Main({Key key}) : super(key: key);

  _MainState createState() => new _MainState();
}

class _MainState extends State<Main> with TickerProviderStateMixin {
  PageController controller = PageController(viewportFraction: 0.8);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.red,
        body: Center(child: Text("Loading")),
      ),
    );
  }
}


