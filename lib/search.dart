import 'package:flutter/material.dart';
import 'package:homecare/home.dart';
import 'package:homecare/locations.dart';
import 'package:homecare/model/user.dart';
import 'package:homecare/providers/user.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';
import 'dart:convert';
import 'package:provider/provider.dart';

class Search extends StatefulWidget {
 final String userName;
  final String userToken;

  Search({this.userName, this.userToken, Key key});
  _SearchState createState() => _SearchState();
}

class City {
  const City(this.name, this.id);

  final String name;
  final String id;
}

class Location {
  const Location(this.name, this.id);

  final String name;
  final String id;
}

class _SearchState extends State<Search> {
  var data;

  List<City> cites = [];
  City cityValue;

  List<Location> locations = [];
  Location locationValue;

  Future<String> getCites() async {
    var response = await http.get(
        Uri.encodeFull("http://delivery.digitalharbor.me//api/city?locale=ar"),
        headers: {"Accept": "application/json"});
    setState(() {
      data = json.decode(response.body);
    });

    for (var i = 0; i < data?.length; i++) {
      cites.insert(
          i, City(data[i]['title'].toString(), data[i]['id'].toString()));
    }

    return "Success";
  }

  Future<String> getLocations(String city) async {
    var response = await http.get(
        Uri.encodeFull("http://delivery.digitalharbor.me//api/city/" +
            city +
            "?locale=ar"),
        headers: {"Accept": "application/json"});
    setState(() {
      data = json.decode(response.body);
      locationValue = null;
      locations.clear();
    });

    for (var i = 0; i < data?.length; i++) {
      locations.insert(
          i, Location(data[i]['title'].toString(), data[i]['id'].toString()));
    }

    return "Success";
  }

  Widget getList() {
    if (data == null || data.length < 1) {
      return Container(
        height: MediaQuery.of(context).size.height * 0.4,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    return Container(
      height: MediaQuery.of(context).size.height * 0.4,
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
        itemCount: data?.length,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.only(left: 8, right: 8, top: 5),
            child: getListItem(index),
          );
        },
      ),
    );
  }

  Widget getListItem(int i) {
    if (data == null || data.length < 1) return null;

    return InkWell(
      child: Container(
        height: 60,
        width: MediaQuery.of(context).size.width * 0.9,
        child: Card(
          color: Color.fromRGBO(58, 95, 126, 1),
          child: Padding(
            padding: EdgeInsets.all(4),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  data[i]['title'].toString(),
                  textAlign: TextAlign.center,
                  textDirection: TextDirection.rtl,
                  style: TextStyle(
                    fontSize: 14,
                    fontFamily: "ar",
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      onTap: () {
        final user = Provider.of<UserModel>(context);

        user.userName = widget.userName;
        user.userToken = widget.userToken;

        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => Locations(
                      city: data[i]['id'].toString(),
                    )));
      },
    );
  }

  @override
  void initState() {
    super.initState();
    getCites();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(
              height: 10,
            ),
            Column(
              children: <Widget>[
                Image.asset(
                  "assets/location.png",
                  width: MediaQuery.of(context).size.width / 3,
                ),

                SizedBox(
                  height: 50,
                ),
                Text(
                  "حدد منطقتك",
                  style: TextStyle(
                      fontSize: 22,
                      fontFamily: "ar",
                      color: Color.fromRGBO(58, 95, 126, 1)),
                ),
                SizedBox(
                  height: 50,
                ),
                //! Cites DropDown
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: 50,
                    decoration: new BoxDecoration(
                        borderRadius: new BorderRadius.all(
                          const Radius.circular(8.0),
                        ),
                        border: Border.all(
                            color: Color.fromRGBO(113, 145, 175, 1))),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: DropdownButton<City>(
                          value: cityValue,
                          iconSize: 24,
                          isExpanded: true,
                          style:
                              TextStyle(color: Colors.black, fontFamily: "rb"),
                          underline: Container(),
                          hint: Text(
                            "المدينة",
                            textAlign: TextAlign.right,
                            textDirection: TextDirection.rtl,
                            style: TextStyle(fontFamily: "rb"),
                          ),
                          onChanged: (City newValue) {
                            setState(() {
                              cityValue = newValue;
                            });
                            getLocations(newValue.id);
                          },
                          items: cites.length < 0
                              ? <City>[City("", "")]
                                  .map<DropdownMenuItem<City>>((City value) {
                                  return DropdownMenuItem<City>(
                                    value: value,
                                    child: Directionality(
                                      textDirection: TextDirection.rtl,
                                      child: Text(
                                        value.name,
                                        textAlign: TextAlign.right,
                                        textDirection: TextDirection.rtl,
                                      ),
                                    ),
                                  );
                                }).toList()
                              : cites.map<DropdownMenuItem<City>>((var value) {
                                  return DropdownMenuItem<City>(
                                    value: value,
                                    child: Directionality(
                                        textDirection: TextDirection.rtl,
                                        child: Text(
                                          value.name,
                                          textAlign: TextAlign.right,
                                          textDirection: TextDirection.rtl,
                                        )),
                                  );
                                }).toList()),
                    ),
                  ),
                ),

                SizedBox(
                  height: 20,
                ),
                //! Locations DropDown

                Visibility(
                  visible: locations.length == 0 ? false : true,
                  child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.8,
                      height: 50,
                      decoration: new BoxDecoration(
                          borderRadius: new BorderRadius.all(
                            const Radius.circular(8.0),
                          ),
                          border: Border.all(
                              color: Color.fromRGBO(113, 145, 175, 1))),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: DropdownButton<Location>(
                            value: locationValue,
                            iconSize: 24,
                            isExpanded: true,
                            style: TextStyle(
                                color: Colors.black, fontFamily: "rb"),
                            underline: Container(),
                            hint: Text(
                              "المنطقة",
                              textAlign: TextAlign.right,
                              textDirection: TextDirection.rtl,
                              style: TextStyle(fontFamily: "rb"),
                            ),
                            onChanged: (Location newValue) {
                              setState(() {
                                locationValue = newValue;
                              });
                            },
                            items: locations.length < 0
                                ? <Location>[Location("", "")]
                                    .map<DropdownMenuItem<Location>>(
                                        (Location value) {
                                    return DropdownMenuItem<Location>(
                                      value: value,
                                      child: Text(value.name),
                                    );
                                  }).toList()
                                : locations.map<DropdownMenuItem<Location>>(
                                    (var value) {
                                    return DropdownMenuItem<Location>(
                                      value: value,
                                      child: Text(value.name),
                                    );
                                  }).toList()),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            InkWell(
                child: Container(
                    decoration: new BoxDecoration(
                      color: Color.fromRGBO(107, 195, 174, 1),
                      borderRadius: new BorderRadius.all(
                        const Radius.circular(5.0),
                      ),
                    ),
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: 50,
                    child: Center(
                      child: Text(
                        "اظهار الخدمات",
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: "ar",
                            color: Colors.white),
                      ),
                    )),
                onTap: () {
                  final user = Provider.of<UserModel>(context);

                  user.userName = widget.userName;
                  user.userToken = widget.userToken;

                  user.locationID = locationValue.id.toString();
                  user.locationTitle = locationValue.name.toString();

                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                }),
            Container()
          ],
        ),
      )),
    );
  }
}
