import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:homecare/color/myColor.dart';
import 'package:homecare/home.dart';
import 'package:homecare/menu.dart';
import 'package:homecare/providers/user.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'dart:convert';
import 'package:badges/badges.dart';

import 'chat.dart';

class ChatList extends StatefulWidget {
  final String category;
  ChatList({this.category, Key key}) : super(key: key);
  _ChatListState createState() => _ChatListState();
}

class _ChatListState extends State<ChatList> {
  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      getData();
    });
    super.initState();
  }

  var data;
  Future<String> getData() async {
    final user = Provider.of<UserModel>(context);

    var response = await http.get(
        Uri.encodeFull("http://delivery.digitalharbor.me//api/msg"),
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer " + user.userToken,
        });
    setState(() {
      data = json.decode(response.body);
    });

    return "Success";
  }

  Widget getChatList() {
    if (data == null || data.length < 1) {
      print('Data:$data');
      return Container(
        height: MediaQuery.of(context).size.height * 0.8,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    if (data['chats'].length == 0) {
      return Container(
        height: MediaQuery.of(context).size.height * 0.8,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: Text('لا يوجد رسائل',style: TextStyle(fontFamily: 'br'),),
        ),
      );
    }
    return Column(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height - 100,
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(top: 20),
          child: ListView.builder(
            itemCount: data['chats']?.length,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding: const EdgeInsets.only(
                    left: 20, right: 20, top: 5, bottom: 5),
                child: InkWell(
                  child: Container(
                    height: 170,
                    child: Badge(
                      badgeColor: Colors.transparent,
                      elevation: 0,
                      badgeContent: Icon(Icons.bookmark,
                          size: 35,
                          color: data['chats'][index]['countNEW'] > 0
                              ? CustomColor.secondartColor
                              : Colors.grey),
                      position: BadgePosition.topLeft(left: 20),
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding:
                                    const EdgeInsets.only(right: 15, top: 15),
                                child: Container(
                                    child: Text(data['chats'][index]['name'],
                                        textAlign: TextAlign.right,
                                        textDirection: TextDirection.rtl,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontFamily: 'br',
                                            color: CustomColor.primaryColor))),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(right: 15),
                                child: Container(
                                    child: Text(data['chats'][index]['title'],
                                        textAlign: TextAlign.right,
                                        textDirection: TextDirection.rtl,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontFamily: 'ar',
                                            color: Colors.grey))),
                              ),
                              Container(
                                padding: EdgeInsets.only(
                                  right: 10,
                                  left: 10,
                                ),
                                height: 48,
                                decoration: BoxDecoration(
                                    color: Colors.grey[100],
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(8),
                                        bottomRight: Radius.circular(8))),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Text(
                                          formatTime(data['chats'][index]
                                                  ['time']
                                              .toString()),
                                          style: TextStyle(
                                            color: Colors.grey,
                                            fontFamily: 'ar',
                                            fontSize: 14,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Icon(
                                          Icons.timer,
                                          size: 17,
                                          color: CustomColor.primaryColor,
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Text(
                                          data['chats'][index]['date']
                                              .toString(),
                                          style: TextStyle(
                                              color: Colors.grey,
                                              fontFamily: 'ar',
                                              fontSize: 14),
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Icon(
                                          Icons.date_range,
                                          size: 17,
                                          color: CustomColor.primaryColor,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Chat(
                                  chat: data['chats'][index]['id'].toString(),
                                )));
                  },
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();

    return Consumer<UserModel>(builder: (context, user, _) {
      return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 4,
          leading: InkWell(
              onTap: () {
                Navigator.pushReplacement(
                    context, MaterialPageRoute(builder: (context) => Home()));
              },
              child: Icon(
                Icons.arrow_back_ios,
                color: CustomColor.primaryColor,
                size: 20,
              )),
          title: Text(
            'الرسائل',
            style: TextStyle(
                fontFamily: 'ar',
                fontSize: 15,
                color: CustomColor.primaryColor),
          ),
          centerTitle: true,
          actions: <Widget>[
            InkWell(
              child: Container(
                child: Image.asset(
                  "assets/menu.png",
                  width: 25,
                  height: 18.3,
                ),
              ),
              onTap: () {
                _scaffoldKey.currentState.openEndDrawer();
              },
            ),
            SizedBox(
              width: 15,
            )
          ],
        ),
        endDrawer: Drawer(child: Menu()),
        body: Container(
          child: Column(
            children: <Widget>[getChatList()],
          ),
        ),
      );
    });
  }

  String formatTime(String string) {
    String d = string.split(':')[0];
    int x = int.parse(d);
    if (x > 12)
      return string.split(':')[0] + ':' + string.split(':')[1] + ' PM';

    return string.split(':')[0] + ':' + string.split(':')[1] + ' AM';
  }
}
