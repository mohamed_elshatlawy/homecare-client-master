import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:homecare/color/myColor.dart';
import 'package:homecare/menu.dart';
import 'package:homecare/providers/user.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'dart:convert';
import 'package:bubble/bubble.dart';
import 'package:intl/intl.dart' as date;

import '../home.dart';
import 'chatlist.dart';
import 'package:auto_size_text/auto_size_text.dart';

class Chat extends StatefulWidget {
  final String chat;
  Chat({this.chat, Key key}) : super(key: key);
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  TextEditingController messageController = new TextEditingController();
  ScrollController _scrollController = new ScrollController();

  static final _formKey = new GlobalKey<FormState>();

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      getData();
    });
    super.initState();
  }

  var messageBack;
  _makeMessageRequest() async {
    final user = Provider.of<UserModel>(context);
    String url = 'https://delivery.digitalharbor.me//api/sendMsg';
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: "application/json",
      HttpHeaders.authorizationHeader: "Bearer " + user.userToken,
    };
    String json = '{"chat_id": "' +
        widget.chat +
        '",  "message":"' +
        messageController.text +
        '"}';
    http.Response response = await http.post(url, headers: headers, body: json);

    setState(() {
      messageBack = response.body;
      // Map valueMap = prefix0.json.decode(body);
      // service = valueMap['service_request_id'].toString();
    });
  }

  var data;
  Future<String> getData() async {
    final user = Provider.of<UserModel>(context);

    var response = await http.get(
        Uri.encodeFull(
            "https://delivery.digitalharbor.me//api/viewMsg/" + widget.chat),
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer " + user.userToken,
        });
    setState(() {
      data = json.decode(response.body);
    });

    return "Success";
  }

  Widget getChatList() {
    if (data == null || data['messages'].length < 1) {
      return Container(
        width: MediaQuery.of(context).size.width,
        child: Center(child: CircularProgressIndicator()),
      );
    }
    return ListView.builder(
      padding: EdgeInsets.all(0),
      controller: _scrollController,
      itemCount: data['messages']?.length,
      shrinkWrap: true,
      reverse: true,
      itemBuilder: (BuildContext context, int index) {
   
        return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        textDirection: data['messages'][index]['type'] == "client"?
        TextDirection.rtl:
        TextDirection.ltr,
        children: <Widget>[
          Container(
            width:  MediaQuery.of(context).size.width * 0.7,
            child: Bubble(
              color: data['messages'][index]['type'] == "client"
                  ? Colors.white
                  : (data['messages'][index]['type'] == "admin"
                      ? Color.fromRGBO(245, 250, 255, 1.0)
                      : Color.fromRGBO(229, 252, 247, 1.0)),
              alignment: data['messages'][index]['type'] == "client"
                  ? Alignment.centerRight
                  : Alignment.centerLeft,
              nip: data['messages'][index]['type'] == "client"
                  ? BubbleNip.rightTop
                  : BubbleNip.leftTop,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        date.DateFormat("yyyy-MM-DD")
                            .format(DateTime.parse(
                                data['messages'][index]['created_at']))
                            .toString(),
                        style: TextStyle(
                            fontSize: 10, color: Colors.grey[500]),
                      ),
                      Text(data['messages'][index]['name'],
                          textAlign: TextAlign.right,
                          style: TextStyle(
                              fontSize: 12,
                              fontFamily: "ar",
                              color: Color.fromRGBO(58, 95, 126, 1))),
                    ],
                  ),
                  Text(data['messages'][index]['message'],
                              textAlign: TextAlign.right,
                              
                         
                              style: TextStyle(fontSize: 14.0)),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Visibility(
                        visible:
                            data['messages'][index]['type_admin'] == 1
                                ? true
                                : false,
                        child: Icon(
                          Icons.check,
                          size: 12,
                          color: Colors.green,
                        ),
                      ),
                      Text(
                        date.DateFormat("hh-mm-ss")
                            .format(DateTime.parse(
                                data['messages'][index]['created_at']))
                            .toString(),
                        style: TextStyle(
                            fontSize: 10, color: Colors.grey[500]),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
      
      
      
     },
    );
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();

    return Consumer<UserModel>(builder: (context, user, _) {
      return Scaffold(
        backgroundColor: Color.fromRGBO(246, 246, 246, 1),
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 4,
          leading: InkWell(
              onTap: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => ChatList()));
              },
              child: Icon(
                Icons.arrow_back_ios,
                color: CustomColor.primaryColor,
                size: 20,
              )),
          title: Text(
            'الرسائل',
            style: TextStyle(
                fontFamily: 'ar',
                fontSize: 15,
                color: CustomColor.primaryColor),
          ),
          centerTitle: true,
          actions: <Widget>[
            InkWell(
              child: Container(
                child: Image.asset(
                  "assets/menu.png",
                  width: 25,
                  height: 18.3,
                ),
              ),
              onTap: () {
                _scaffoldKey.currentState.openEndDrawer();
              },
            ),
            SizedBox(
              width: 15,
            )
          ],
        ),
        endDrawer: Drawer(child: Menu()),
        body: Container(
          child: Column(
            children: <Widget>[
              Expanded(child: getChatList()),
              Container(
                color: Colors.white,
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.only(
                      right: 20, left: 20, top: 15, bottom: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Form(
                        key: _formKey,
                        child: Expanded(
                          child: Container(
                            height: 45,
                            padding: EdgeInsets.only(right: 15),
                            decoration: BoxDecoration(
                              border: Border.all(width: 1, color: Colors.grey),
                              borderRadius: BorderRadius.circular(30),
                            ),
                            child: TextField(
                              controller: messageController,
                              textDirection: TextDirection.rtl,
                              textAlign: TextAlign.right,
                              decoration: new InputDecoration(
                                  border: InputBorder.none,
                                  hintText: 'رد على الرسالة',
                                  hintStyle: TextStyle(
                                      fontFamily: "ar",
                                      fontSize: 12,
                                      color:
                                          Color.fromRGBO(138, 138, 138, 0.5))),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      InkWell(
                        child: CircleAvatar(
                          backgroundColor: CustomColor.primaryColor,
                          child: Image.asset('assets/send.png'),
                        ),
                        onTap: () {
                          _makeMessageRequest();
                          getData();
                          messageController.clear();
                          FocusScope.of(context).requestFocus(new FocusNode());
                          _scrollController.animateTo(
                            0.0,
                            curve: Curves.easeOut,
                            duration: const Duration(milliseconds: 300),
                          );
                        },
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      );
    });
  }
}
