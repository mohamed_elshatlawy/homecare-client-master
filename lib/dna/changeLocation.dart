import 'package:flutter/material.dart';
import 'package:homecare/providers/user.dart';
import 'package:homecare/search.dart';
import 'package:provider/provider.dart';

class ChangeLocation extends StatefulWidget {
  ChangeLocation({Key key}) : super(key: key);

  @override
  _ChangeLocationState createState() => _ChangeLocationState();
}

class _ChangeLocationState extends State<ChangeLocation> {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserModel>(context);

    return Container(
        width: MediaQuery.of(context).size.width,
        height: 50,
        color: Color.fromRGBO(226, 241, 255, 1),
        child: Padding(
          padding: const EdgeInsets.only(left: 30, right: 30),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              RaisedButton(
                color: Colors.white,
                child: Text(
                  "تغيير الموقع",
                  style: TextStyle(
                    fontFamily: "ar",
                    fontSize: 12,
                    color: Color.fromRGBO(48, 83, 121, 1),
                  ),
                ),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Search()));
                
                
                },
              ),
              Row(
                children: <Widget>[
                  Text(
                    user.locationTitle,
                    style: TextStyle(
                      fontFamily: "ar",
                      fontWeight: FontWeight.w900,
                      fontSize: 11,
                      color: Color.fromRGBO(48, 83, 121, 1),
                    ),
                  ),
                  Text(
                    " عرض الفئات في ",
                    style: TextStyle(
                      fontFamily: "ar",
                      fontWeight: FontWeight.w100,
                      fontSize: 11,
                      color: Color.fromRGBO(48, 83, 121, 1),
                    ),
                  ),
                ],
              )
            ],
          ),
        ));
  }
}
