import 'package:flutter/material.dart';

Widget getErrorWidget(BuildContext context, FlutterErrorDetails error) {
  return Scaffold(
    body: Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      // decoration: new BoxDecoration(
      //     gradient: LinearGradient(
      //         begin: Alignment.topLeft,
      //         end: Alignment.bottomRight,
      //         colors: [
      //           Color(0xff3590e2),
      //           Color(0xff292f77),
      //         ]),
      //     borderRadius: new BorderRadius.only(
      //       bottomLeft: const Radius.circular(0.0),
      //       bottomRight: const Radius.circular(0.0),
      //     )),
      child: Center(child: CircularProgressIndicator()),
    ),
  );
}
