import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:homecare/dna/changeLocation.dart';
import 'package:homecare/menu.dart';
import 'package:homecare/news/news.dart';
import 'package:homecare/search.dart';
import 'package:homecare/services.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_messaging/firebase_messaging.dart';
import 'color/myColor.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var data;
final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
static Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
    if (message.containsKey('data')) {
      // Handle data message
      final dynamic data = message['data'];
      print('BackGroundDataFCM:$data');
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
      print('BackgroundNotificationFCM:$notification');
    }

    // Or do other work.
  }

  
setupFCM(){
  _firebaseMessaging.getToken().then((tok){
    print('Token:$tok');
  });
  _firebaseMessaging.configure(
       onMessage: (Map<String, dynamic> message) async {
         print("onMessage: $message");
 //        _showItemDialog(message);
          /*Navigator.push(context, MaterialPageRoute(
            builder: (context)=>News()
          ));*/
       },
       onBackgroundMessage: myBackgroundMessageHandler,
       onLaunch: (Map<String, dynamic> message) async {
         print("onLaunch: $message");
    //     _navigateToItemDetail(message);
     Navigator.push(context, MaterialPageRoute(
            builder: (context)=>News()
          ));
       },
       onResume: (Map<String, dynamic> message) async {
         print("onResume: $message");
  //         _navigateToItemDetail(message);
   Navigator.push(context, MaterialPageRoute(
            builder: (context)=>News()
          ));
       },
     );
}
  @override
  void initState() {
    super.initState();
    getData();
    setupFCM();
  }

  Future<String> getData() async {
    var response = await http.get(
        Uri.encodeFull(
            "http://delivery.digitalharbor.me//api/category?locale=ar"),
        headers: {"Accept": "application/json"});
    setState(() {
      data = json.decode(response.body);
    });
    return "Success";
  }

  Widget getList() {
    if (data == null || data.length < 1) {
      return Container(
        height: MediaQuery.of(context).size.height * 0.8,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    return Container(
      height: MediaQuery.of(context).size.height * 0.8,
      width: MediaQuery.of(context).size.width,
      child: GridView.builder(
          itemCount: data?.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: (MediaQuery.of(context).size.width / 2 / 180),
          ),
          itemBuilder: (BuildContext context, int index) {
            return getListItem(index);
          }),
    );
  }

  Widget getListItem(int i) {
    if (data == null || data.length < 1) return null;

    return InkWell(
      child: Container(
        height: 40,
        width: 40,
        child: Padding(
          padding: EdgeInsets.all(4),
          child: Column(
            children: <Widget>[
              FadeInImage.assetNetwork(
                placeholder: 'assets/holder.png',
                image: data[i]['image_link'].toString(),
                width: 119,
                height: 119,
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                data[i]['title'].toString(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 14,
                  fontFamily: "ar",
                  color: Color.fromRGBO(48, 83, 121, 1),
                ),
              ),
            ],
          ),
        ),
      ),
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => Services(
                      category: data[i]['id'].toString(),
                    )));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>(); // ADD THIS LINE

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: Colors.white,
        key: _scaffoldKey,
        endDrawer: Drawer(child: Menu()),
        appBar: PreferredSize(
          child: AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            leading: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(
                  Icons.arrow_back_ios,
                  color: CustomColor.primaryColor,
                  size: 20,
                )),
            title: Text(
              'الاقسام',
              style: TextStyle(
                  fontFamily: 'ar',
                  fontSize: 15,
                  color: CustomColor.primaryColor),
            ),
            centerTitle: true,
            actions: <Widget>[
              InkWell(
                child: Container(
                  child: Image.asset(
                    "assets/menu.png",
                    width: 25,
                    height: 18.3,
                  ),
                ),
                onTap: () {
                  _scaffoldKey.currentState.openEndDrawer();
                },
              ),
              SizedBox(
                width: 15,
              )
            ],
          ),
          preferredSize: Size.fromHeight(60),
        ),
        body: Container(
          child: Column(
            children: <Widget>[
              ChangeLocation(),
              SizedBox(
                height: 20,
              ),
              Expanded(child: getList())
            ],
          ),
        ),
      ),
    );
  }
}
