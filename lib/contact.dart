import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:homecare/menu.dart';
import 'package:homecare/providers/user.dart';
import 'package:provider/provider.dart';

import 'color/myColor.dart';
import 'home.dart';

class Contact extends StatefulWidget {
  final String category;
  Contact({this.category, Key key}) : super(key: key);
  _ContactState createState() => _ContactState();
}

class _ContactState extends State<Contact> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>(); // ADD THIS LINE

    return Consumer<UserModel>(builder: (context, user, _) {
      return Scaffold(
        backgroundColor: Colors.white,
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          leading: InkWell(
              onTap: () {
                Navigator.pushReplacement(
                    context, MaterialPageRoute(builder: (context) => Home()));
              },
              child: Icon(
                Icons.arrow_back_ios,
                color: CustomColor.primaryColor,
                size: 20,
              )),
          title: Text(
            'اتصل بنا',
            style: TextStyle(
                fontFamily: 'ar',
                fontSize: 15,
                color: CustomColor.primaryColor),
          ),
          centerTitle: true,
        ),
        endDrawer: Drawer(child: Menu()),
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 25,
                ),
                Text(
                  "الخط الساخن",
                  style: TextStyle(
                      fontFamily: "ar",
                      fontSize: 18,
                      color: Color.fromRGBO(107, 195, 174, 1)),
                ),
                Text(
                  "19519",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 50,
                      color: Color.fromRGBO(58, 95, 126, 1)),
                ),
                SizedBox(
                  height: 35,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                     
                      children: <Widget>[
                        Text(
                          "ارقام التليفون",
                          style: TextStyle(
                              fontFamily: "ar",
                              fontSize: 16,
                              color: Color.fromRGBO(58, 95, 126, 1)),
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        
                        Row(
                          textDirection: TextDirection.rtl,
                          children: <Widget>[
                            
                            CircleAvatar(
                              radius: 13,
                              backgroundColor: CustomColor.secondartColor,
                              child: Icon(
                                Icons.call,
                                color: Colors.white,
                                size: 15,
                                textDirection: TextDirection.ltr,
                              ),
                            ),
                            SizedBox(
                              width: 15,
                            ),
                            Text(
                              '1234 - 1234 - 1234',
                              style: TextStyle(
                                  fontFamily: 'ar',
                                  fontSize: 16,
                                  color: Colors.grey[600],
                                  fontWeight: FontWeight.w500),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          textDirection: TextDirection.rtl,
                          children: <Widget>[
                            CircleAvatar(
                              radius: 13,
                              backgroundColor: CustomColor.secondartColor,
                              child: Icon(
                                Icons.call,
                                color: Colors.white,
                                size: 15,
                                textDirection: TextDirection.ltr,
                              ),
                            ),
                            SizedBox(
                              width: 15,
                            ),
                            Text(
                              '1234 - 1234 - 1234',
                              style: TextStyle(
                                  fontFamily: 'ar',
                                  fontSize: 16,
                                  color: Colors.grey[600],
                                  fontWeight: FontWeight.w500),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          textDirection: TextDirection.rtl,
                          children: <Widget>[
                            CircleAvatar(
                              radius: 13,
                              backgroundColor: CustomColor.secondartColor,
                              child: Icon(
                                Icons.call,
                                color: Colors.white,
                                size: 15,
                                textDirection: TextDirection.ltr,
                              ),
                            ),
                            SizedBox(
                              width: 15,
                            ),
                            Text(
                              '1234 - 1234 - 1234',
                              style: TextStyle(
                                  fontFamily: 'ar',
                                  fontSize: 16,
                                  color: Colors.grey[600],
                                  fontWeight: FontWeight.w500),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        Text(
                          "البريد الالكتروني",
                          style: TextStyle(
                              fontFamily: "ar",
                              fontSize: 16,
                              color: Color.fromRGBO(58, 95, 126, 1)),
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        Row(
                          textDirection: TextDirection.rtl,
                          children: <Widget>[
                            CircleAvatar(
                              radius: 13,
                              backgroundColor: CustomColor.secondartColor,
                              child: Icon(
                                Icons.alternate_email,
                                color: Colors.white,
                                size: 15,
                                textDirection: TextDirection.ltr,
                              ),
                            ),
                            SizedBox(
                              width: 15,
                            ),
                            Text(
                              'name@nafa.com.sa',
                              style: TextStyle(
                                  fontFamily: 'ar',
                                  fontSize: 16,
                                  color: Colors.grey[600],
                                  fontWeight: FontWeight.w500),
                            )
                          ],
                        ),
                      SizedBox(height: 10,),
                        Row(
                          textDirection: TextDirection.rtl,
                          children: <Widget>[
                            CircleAvatar(
                              radius: 13,
                              backgroundColor: CustomColor.secondartColor,
                              child: Icon(
                                Icons.alternate_email,
                                color: Colors.white,
                                size: 15,
                                textDirection: TextDirection.ltr,
                              ),
                            ),
                            SizedBox(
                              width: 15,
                            ),
                            Text(
                              'name@nafa.com.sa',
                              style: TextStyle(
                                  fontFamily: 'ar',
                                  fontSize: 16,
                                  color: Colors.grey[600],
                                  fontWeight: FontWeight.w500),
                            )
                          ],
                        ),
                       SizedBox(height: 10,),
                        Row(
                          textDirection: TextDirection.rtl,
                          children: <Widget>[
                            CircleAvatar(
                              radius: 13,
                              backgroundColor: CustomColor.secondartColor,
                              child: Icon(
                                Icons.alternate_email,
                                color: Colors.white,
                                size: 15,
                                textDirection: TextDirection.ltr,
                              ),
                            ),
                            SizedBox(
                              width: 15,
                            ),
                            Text(
                              'name@nafa.com.sa',
                              style: TextStyle(
                                  fontFamily: 'ar',
                                  fontSize: 16,
                                  color: Colors.grey[600],
                                  fontWeight: FontWeight.w500),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        Text(
                          "العناوين",
                          style: TextStyle(
                              fontFamily: "ar",
                              fontSize: 16,
                              color: Color.fromRGBO(58, 95, 126, 1)),
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        Row(
                          textDirection: TextDirection.rtl,
                          children: <Widget>[
                            CircleAvatar(
                              radius: 13,
                              backgroundColor: CustomColor.secondartColor,
                              child: Icon(
                                Icons.location_on,
                                color: Colors.white,
                                size: 15,
                                textDirection: TextDirection.ltr,
                              ),
                            ),
                            SizedBox(
                              width: 15,
                            ),
                            Flexible(
                              child: Text(
                                '6789-Alshekh mubarak ,riyad 123 ,saudi Arabia',
                                textAlign: TextAlign.right,
                                style: TextStyle(
                                    fontFamily: 'ar',
                                    fontSize: 16,
                                    color: Colors.grey[600],
                                    fontWeight: FontWeight.w500),
                              ),
                            )
                          ],
                        ),
                     SizedBox(height: 10,),
                        Row(
                          textDirection: TextDirection.rtl,
                          children: <Widget>[
                            CircleAvatar(
                              radius: 13,
                              backgroundColor: CustomColor.secondartColor,
                              child: Icon(
                                Icons.location_on,
                                color: Colors.white,
                                size: 15,
                                textDirection: TextDirection.ltr,
                              ),
                            ),
                            SizedBox(
                              width: 15,
                            ),
                            Flexible(
                              child: Text(
                                '6789-Alshekh mubarak ,riyad 123 ,saudi Arabia',
                                textAlign: TextAlign.right,
                                style: TextStyle(
                                    fontFamily: 'ar',
                                    fontSize: 16,
                                    color: Colors.grey[600],
                                    fontWeight: FontWeight.w500),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 25,
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      );
    });
  }
}
