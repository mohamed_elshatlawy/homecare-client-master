import 'package:flutter/material.dart';

class Lang extends StatelessWidget {
  const Lang({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/lang.png",
              width: MediaQuery.of(context).size.width / 2,
            ),
            Text(
              "اختر اللغة المفضلة",
              style: TextStyle(
                  fontSize: 22,
                  fontFamily: "ar",
                  color: Color.fromRGBO(58, 95, 126, 1)),
            ),
            SizedBox(
              height: 80,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 80,
                  width: 80,
                  child: FloatingActionButton(
                    heroTag: "en",
                    backgroundColor: Color.fromRGBO(204, 227, 255, 1),
                    elevation: 0,
                    child: Text(
                      "EN",
                      style: TextStyle(
                          fontSize: 22,
                          color: Color.fromRGBO(58, 95, 126, 1),
                          fontWeight: FontWeight.bold),
                    ),
                    onPressed: () => {},
                  ),
                ),
                SizedBox(
                  width: 100,
                ),
                Container(
                  height: 80,
                  width: 80,
                  child: FloatingActionButton(
                    heroTag: "ar",
                    child: Text(
                      "ع",
                      style: TextStyle(
                          fontSize: 28,
                          color: Color.fromRGBO(58, 95, 126, 1),
                          fontFamily: "calibri",
                          fontWeight: FontWeight.bold),
                    ),
                    backgroundColor: Color.fromRGBO(204, 227, 255, 1),
                    elevation: 0,
                    onPressed: () => {},
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
