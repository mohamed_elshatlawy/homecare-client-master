import 'dart:io';

import 'package:flutter/material.dart';
import 'package:homecare/access/account.dart';
import 'package:homecare/access/login.dart';
import 'package:homecare/chat/chatlist.dart';
import 'package:homecare/contact.dart';
import 'package:homecare/home.dart';
import 'package:homecare/howToUse.dart';
import 'package:homecare/news/news.dart';
import 'package:homecare/order/attach.dart';
import 'package:homecare/order/orders.dart';
import 'package:homecare/providers/switchProvider.dart';
import 'package:homecare/providers/user.dart';
import 'package:homecare/terms.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:badges/badges.dart';
import 'package:provider/provider.dart';
import 'color/myColor.dart';

class Menu extends StatefulWidget {
  Menu({Key key}) : super(key: key);

  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  var data;

  Future<String> getNotifications() async {
    final user = Provider.of<UserModel>(context);

    var response = await http.get(
        Uri.encodeFull("http://delivery.digitalharbor.me//api/notification"),
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer " + user.userToken,
        });
    setState(() {
      data = json.decode(response.body);
    });
    return "Success";
  }

  String imgURL = "", name;

  Future<String> getData() async {
    final user = Provider.of<UserModel>(context);

    var response = await http.get(
        Uri.encodeFull("http://delivery.digitalharbor.me//api/profile"),
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer " + user.userToken,
        });
    setState(() {
      data = json.decode(response.body);

      imgURL = data['image_link'];

      print('Link:$imgURL');
    });

    return "Success";
  }

  bool notify = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    getData();
    getNotifcationSharedPref().then((v) {
      if (v == true) {
        notify = true;
        setState(() {});
        getNotifications();
      }
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(builder: (context, user, _) {
      return Container(
          color: Color.fromRGBO(48, 83, 121, 1),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 43.0),
                  child: Container(
                    height: 59,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          "القائمة",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 16,
                              fontFamily: "ar",
                              color: Colors.white),
                        )),
                        Container(
                          margin: EdgeInsets.only(right: 10),
                          child: InkWell(
                            child: Icon(
                              Icons.close,
                              color: Colors.white,
                              size: 30,
                            ),
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.8,
                  child: ListView(
                    children: <Widget>[
                      Center(
                        child: Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                  color: CustomColor.primaryColor, width: 2.5)),
                          child: CircleAvatar(
                            radius: 40,
                            backgroundImage: (imgURL == "")
                                ? AssetImage(
                                    'assets/profile.png',
                                  )
                                : NetworkImage(imgURL),
                            backgroundColor: Colors.white,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => OrderAttach()));
                              },
                              child: Text(
                                user.userName ?? "",
                                textAlign: TextAlign.right,
                                style: TextStyle(
                                    fontSize: 16,
                                    fontFamily: "ar",
                                    color: Colors.white),
                              ),
                            )
                          ],
                        ),
                      ),
                      ListTile(
                        title: Text(
                          "طلباتى",
                          textAlign: TextAlign.right,
                          style: TextStyle(
                              fontSize: 12,
                              fontFamily: "ar",
                              color: Colors.white),
                        ),
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Orders()));
                        },
                      ),
                      InkWell(
                        child: ListTile(
                          title: Text(
                            "حسابي",
                            textAlign: TextAlign.right,
                            style: TextStyle(
                                fontSize: 12,
                                fontFamily: "ar",
                                color: Colors.white),
                          ),
                        ),
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      ChangeNotifierProvider.value(
                                          value: new SwitchProvider(),
                                          child: Account())));
                        },
                      ),
                      InkWell(
                        child: Badge(
                          showBadge: (notify == false)
                              ? false
                              : (data == null ||
                                      data['newsletter'].toString() == "0")
                                  ? false
                                  : true,
                          badgeColor: Colors.red,
                          badgeContent: Text(
                            data == null
                                ? "0"
                                : (data['newsletter'].toString() == "null")
                                    ? "0"
                                    : data['newsletter'].toString(),
                            style: TextStyle(color: Colors.white),
                          ),
                          position: BadgePosition.topLeft(left: 20, top: 10),
                          child: ListTile(
                            title: Text(
                              "التنبيهات",
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                  fontSize: 12,
                                  fontFamily: "ar",
                                  color: Colors.white),
                            ),
                          ),
                        ),
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) => News()));
                        },
                      ),
                      Badge(
                        showBadge:
                            (data == null || data['chat'].toString() == "0")
                                ? false
                                : true,
                        badgeColor:
                            (data == null) ? Colors.transparent : Colors.red,
                        badgeContent: Text(
                          data == null
                              ? "0"
                              : (data['chat'].toString() == "null")
                                  ? "0"
                                  : data['chat'].toString(),
                          style: TextStyle(color: Colors.white),
                        ),
                        position: BadgePosition.topLeft(left: 20, top: 10),
                        child: InkWell(
                          child: ListTile(
                            title: Text(
                              "الرسائل",
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                  fontSize: 12,
                                  fontFamily: "ar",
                                  color: Colors.white),
                            ),
                          ),
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ChatList()));
                          },
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(left: 20, right: 20),
                          child: MySeparator(height: 1, color: Colors.white)),
                      SizedBox(
                        height: 10,
                      ),
                      InkWell(
                        child: ListTile(
                          title: Text(
                            "الرئيسية",
                            textAlign: TextAlign.right,
                            style: TextStyle(
                                fontSize: 12,
                                fontFamily: "ar",
                                color: Colors.white),
                          ),
                        ),
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) => Home()));
                        },
                      ),
                      InkWell(
                        child: ListTile(
                          title: Text(
                            "كيفية الاستخدام",
                            textAlign: TextAlign.right,
                            style: TextStyle(
                                fontSize: 12,
                                fontFamily: "ar",
                                color: Colors.white),
                          ),
                        ),
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HowToUse()));
                        },
                      ),
                      InkWell(
                        child: ListTile(
                          title: Text(
                            "الشروط والاحكام",
                            textAlign: TextAlign.right,
                            style: TextStyle(
                                fontSize: 12,
                                fontFamily: "ar",
                                color: Colors.white),
                          ),
                        ),
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) => Terms()));
                        },
                      ),
                      InkWell(
                        child: ListTile(
                          title: Text(
                            "اتصل بنا",
                            textAlign: TextAlign.right,
                            style: TextStyle(
                                fontSize: 12,
                                fontFamily: "ar",
                                color: Colors.white),
                          ),
                        ),
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Contact()));
                        },
                      ),
                      InkWell(
                        child: ListTile(
                          title: Text(
                            "تسجيل خروج",
                            textAlign: TextAlign.right,
                            style: TextStyle(
                                fontSize: 12,
                                fontFamily: "ar",
                                color: Color.fromRGBO(107, 195, 174, 1)),
                          ),
                        ),
                        onTap: () async {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();

                          prefs.remove('name');
                          prefs.remove('token');

                          Navigator.pushReplacement(context,
                              MaterialPageRoute(builder: (context) => Login()));
                        },
                      ),
                    ],
                  ),
                )
              ],
            ),
          ));
    });
  }
}

class MySeparator extends StatelessWidget {
  final double height;
  final Color color;

  const MySeparator({this.height = 1, this.color = Colors.black});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        final dashWidth = 5.0;
        final dashHeight = height;
        final dashCount = (boxWidth / (2 * dashWidth)).floor();
        return Flex(
          children: List.generate(dashCount, (_) {
            return SizedBox(
              width: dashWidth,
              height: dashHeight,
              child: DecoratedBox(
                decoration: BoxDecoration(color: color),
              ),
            );
          }),
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          direction: Axis.horizontal,
        );
      },
    );
  }
}
