import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SwitchProvider extends ChangeNotifier {
  bool switchValue = true;
  SwitchProvider() {
    getNotifcationSharedPref().then((v) {
      print('SwitchV:$v');
      switchValue = v;
     
    });
  }
  switchNotification(bool v) {
    switchValue = v;
     saveNotificationSharedPref(v).then((v1) {
        notifyListeners();
      });
    //notifyListeners();
  }
}

Future<void> saveNotificationSharedPref(bool v) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool('notify', v);
}

Future<bool> getNotifcationSharedPref() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if(!prefs.containsKey("notify")){
   await prefs.setBool("notify", true);
  }

  return prefs.getBool('notify') ;
}
