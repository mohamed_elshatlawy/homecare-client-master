import 'package:flutter/material.dart';

class UserModel with ChangeNotifier {
  String _locationID = "";
  String get locationID => _locationID;
  set locationID(String name) {
    _locationID = name;
    notifyListeners();
  }

  String _locationTitle = "";
  String get locationTitle => _locationTitle;
  set locationTitle(String name) {
    _locationTitle = name;
    notifyListeners();
  }

  String _userName = "";
  String get userName => _userName;
  set userName(String name) {
    _userName = name;
    notifyListeners();
  }

  String _userToken = "";
  String get userToken => _userToken;
  set userToken(String name) {
    _userToken = name;
    notifyListeners();
  }
}
