import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:homecare/menu.dart';
import 'package:homecare/providers/user.dart';
import 'package:provider/provider.dart';

class Blank extends StatefulWidget {
  final String category;
  Blank({this.category, Key key}) : super(key: key);
  _BlankState createState() => _BlankState();
}

class _BlankState extends State<Blank> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>(); // ADD THIS LINE

    return Consumer<UserModel>(builder: (context, user, _) {
      return Scaffold(
        key: _scaffoldKey,
        endDrawer: Drawer(child: Menu()),
        body: Container(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(
                    top: 30, left: 10, right: 10, bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(
                      Icons.chevron_left,
                      color: Color.fromRGBO(48, 83, 121, 1),
                    ),
                    Text(
                      "اتصل بنا",
                      style: TextStyle(
                          fontSize: 20,
                          fontFamily: "ar",
                          color: Color.fromRGBO(58, 95, 126, 1)),
                    ),
                    InkWell(
                      child: Image.asset(
                        "assets/menu.png",
                        width: 24,
                      ),
                      onTap: () {
                        _scaffoldKey.currentState.openEndDrawer();
                      },
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
}
