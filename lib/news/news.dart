import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:homecare/color/myColor.dart';
import 'package:homecare/home.dart';
import 'package:homecare/menu.dart';
import 'package:homecare/news/newsview.dart';
import 'package:homecare/providers/user.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class News extends StatefulWidget {
  final String category;
  News({this.category, Key key}) : super(key: key);
  _NewsState createState() => _NewsState();
}

class _NewsState extends State<News> {
  var data;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      getData();
    });
  }

  Future<String> getData() async {
    final user = Provider.of<UserModel>(context);

    var response = await http.get(
        Uri.encodeFull("http://delivery.digitalharbor.me//api/NewsLetter"),
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer " + user.userToken,
        });
    setState(() {
      data = json.decode(response.body);
    });
    return "Success";
  }

  Widget getList() {
    if (data == null || data.length < 1) {
      print('Data:$data');
      return Container(
        height: MediaQuery.of(context).size.height * 0.8,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    if (data['messages'].length == 0) {
      return Container(
        height: MediaQuery.of(context).size.height * 0.8,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: Text('لا يوجد تنبيهات'),
        ),
      );
    }
    return Container(
      height: MediaQuery.of(context).size.height - 100,
      width: MediaQuery.of(context).size.width,
      color: Colors.grey.withOpacity(0.1),
      child: ListView.builder(
        itemCount: data['messages']?.length,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.only(left: 8, right: 8, top: 5),
            child: getListItem(index),
          );
        },
      ),
    );
  }

  Map<String, String> mappingMonths = {
    '01': 'يناير',
    '02': 'فبراير',
    '03': 'مارس',
    '04': 'ابريل',
    '05': 'مايو',
    '06': 'يونيو',
    '07': 'يوليو',
    '08': 'اغسطس',
    '09': 'سبتمبر',
    '10': 'اكتوبر',
    '11': 'نوفمبر',
    '12': 'ديسمبر'
  };

  String mappingDate(String date) {
    print(date);
    String d = date.split(' ')[0];
    print(d);
    String month = d.split('-')[1];
    String day = d.split('-')[2];

    String m = mappingMonths[month];
    print(m);
    return m + ' ' + day;
  }

  Widget getListItem(int i) {
    if (data == null || data.length < 1) return null;

    return InkWell(
      child: Container(
        child: Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          elevation: 8,
          child: Padding(
            padding: EdgeInsets.all(0),
            child: Column(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.white,
                  ),
                  height: 130,
                  child: ClipRRect(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(8),
                          topRight: Radius.circular(8)),
                      child: FadeInImage.assetNetwork(
                        placeholder: 'assets/service.png',
                        image: data['messages'][i]['image'].toString(),
                        fit: BoxFit.fill,
                      )),
                ),
             
                SizedBox(
                  height: 10,
                ),
             
                Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  height: 140,
                  child: Column(
                    children: <Widget>[
                      Row(
                        textDirection: TextDirection.rtl,
                        children: <Widget>[
                          Text(
                            mappingDate(
                                data['messages'][i]['created_at'].toString()),
                            textDirection: TextDirection.rtl,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              fontSize: 11,
                              fontFamily: "ar",
                              fontWeight: FontWeight.bold,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        textDirection: TextDirection.rtl,
                        children: <Widget>[
                          Flexible(
                                                      child: Text(
                              data['messages'][i]['title'].toString(),
                              textDirection: TextDirection.rtl,
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                fontSize: 14,
                                fontFamily: "ar",
                                fontWeight: FontWeight.bold,
                                color: Color.fromRGBO(58, 95, 126, 1),
                              ),
                            ),
                          ),
                        ],
                      ),
              
                      Row(
                        textDirection: TextDirection.rtl,
                        children: <Widget>[
                          Flexible(
                            child: RichText(
                              textAlign: TextAlign.right,
                              strutStyle: StrutStyle(fontSize: 12.0),
                              overflow: TextOverflow.ellipsis,
                              text: TextSpan(
                                style: TextStyle(
                                  fontSize: 12,
                                  fontFamily: "rb",
                                  color: Color.fromRGBO(158, 158, 158, 1),
                                ),
                                text: data['messages'][i]['message'].toString(),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.arrow_forward,
                            color: CustomColor.secondartColor,
                            textDirection: TextDirection.rtl,
                            size: 16,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          InkWell(
                            child: Text(
                              "اقراء اكثر",
                              textDirection: TextDirection.rtl,
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: 11,
                                fontFamily: "ar",
                                fontWeight: FontWeight.bold,
                                color: CustomColor.secondartColor,
                              ),
                            ),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => NewsView(
                                            newsId: data['messages'][i]['id']
                                                .toString(),
                                          )));
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
        
              ],
            ),
          ),
        ),
      ),
      onTap: () {},
    );
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>(); // ADD THIS LINE

    return Consumer<UserModel>(builder: (context, user, _) {
      return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          leading: InkWell(
              onTap: () {
                Navigator.pushReplacement(
                    context, MaterialPageRoute(builder: (context) => Home()));
              },
              child: Icon(
                Icons.arrow_back_ios,
                color: CustomColor.primaryColor,
                size: 20,
              )),
          title: Text(
            'التنبيهات',
            style: TextStyle(
                fontFamily: 'ar',
                fontSize: 15,
                color: CustomColor.primaryColor),
          ),
          centerTitle: true,
          actions: <Widget>[
            InkWell(
              child: Container(
                child: Image.asset(
                  "assets/menu.png",
                  width: 25,
                  height: 18.3,
                ),
              ),
              onTap: () {
                _scaffoldKey.currentState.openEndDrawer();
              },
            ),
            SizedBox(
              width: 15,
            )
          ],
        ),
        endDrawer: Drawer(child: Menu()),
        body: Container(
          child: Column(
            children: <Widget>[getList()],
          ),
        ),
      );
    });
  }
}
