import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:homecare/color/myColor.dart';
import 'package:homecare/dna/error.dart';
import 'package:homecare/menu.dart';
import 'package:homecare/news/news.dart';
import 'package:homecare/providers/user.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class NewsView extends StatefulWidget {
  final String newsId;
  NewsView({this.newsId, Key key}) : super(key: key);

  @override
  _NewsViewState createState() => _NewsViewState();
}

TextEditingController problemController = new TextEditingController();

class _NewsViewState extends State<NewsView> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      getData();
    });
  }

  var data;
  Future<String> getData() async {
    final user = Provider.of<UserModel>(context);

    var response = await http.get(
        Uri.encodeFull("http://delivery.digitalharbor.me//api/oneNewsLetter/" +
            widget.newsId +
            "?locale=ar"),
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer " + user.userToken,
        });
    setState(() {
      data = json.decode(response.body);
    });
    //print(user.userToken);

    return "Success";
  }

  Map<String, String> mappingMonths = {
    '01': 'يناير',
    '02': 'فبراير',
    '03': 'مارس',
    '04': 'ابريل',
    '05': 'مايو',
    '06': 'يونيو',
    '07': 'يوليو',
    '08': 'اغسطس',
    '09': 'سبتمبر',
    '10': 'اكتوبر',
    '11': 'نوفمبر',
    '12': 'ديسمبر'
  };

  String mappingDate(String date) {
    print(date);
    String d = date.split(' ')[0];
    print(d);
    String month = d.split('-')[1];
    String day = d.split('-')[2];

    String m = mappingMonths[month];
    print(m);
    return m + ' ' + day;
  }

  @override
  Widget build(BuildContext context) {
    ErrorWidget.builder = (FlutterErrorDetails errorDetails) {
      return getErrorWidget(context, errorDetails);
    };

    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();

    return MaterialApp(
        title: 'location picker',
        supportedLocales: const <Locale>[
          Locale('en', ''),
          Locale('ar', ''),
        ],
        home: Scaffold(
            key: _scaffoldKey,
            appBar: AppBar(
              backgroundColor: Colors.white,
              elevation: 0,
              leading: InkWell(
                  onTap: () {
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) => News()));
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: CustomColor.primaryColor,
                    size: 20,
                  )),
              title: Text(
                data['message']['title'].toString(),
                style: TextStyle(
                    fontFamily: 'ar',
                    fontSize: 15,
                    color: CustomColor.primaryColor),
              ),
              centerTitle: true,
              actions: <Widget>[
                InkWell(
                  child: Container(
                    child: Image.asset(
                      "assets/menu.png",
                      width: 25,
                      height: 18.3,
                    ),
                  ),
                  onTap: () {
                    _scaffoldKey.currentState.openEndDrawer();
                  },
                ),
                SizedBox(
                  width: 15,
                )
              ],
            ),
            endDrawer: Drawer(child: Menu()),
            body: Builder(builder: (context) {
              return Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height - 100,
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 10, right: 10, top: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  height: 200,
                                  width: MediaQuery.of(context).size.width,
                                  child: FadeInImage.assetNetwork(
                                    placeholder: 'assets/service.png',
                                    image: data['message']['image'].toString(),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                Expanded(
                                  child: Container(
                                      child: SingleChildScrollView(
                                    child: Column(children: <Widget>[
                                      Row(
                                        textDirection: TextDirection.rtl,
                                        children: <Widget>[
                                          Flexible(
                                                                                      child: Text(
                                              data['message']['title'].toString(),
                                              textDirection: TextDirection.rtl,
                                              textAlign: TextAlign.right,
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontFamily: "ar",
                                                fontWeight: FontWeight.bold,
                                                color: Color.fromRGBO(58, 95, 126, 1),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        textDirection: TextDirection.rtl,
                                        children: <Widget>[
                                          Text(
                                            mappingDate(data['message']
                                                    ['created_at']
                                                .toString()),
                                            textDirection: TextDirection.rtl,
                                            textAlign: TextAlign.right,
                                            style: TextStyle(
                                              fontSize: 11,
                                              fontFamily: "ar",
                                              fontWeight: FontWeight.bold,
                                              color: CustomColor.secondartColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        data['message']['message'].toString(),
                                        textDirection: TextDirection.rtl,
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                          fontSize: 14,
                                          fontFamily: "rb",
                                          fontWeight: FontWeight.bold,
                                          color: Colors.grey,
                                        ),
                                      ),
                                      Text(
                                        data['message']['message'].toString(),
                                        textDirection: TextDirection.rtl,
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                          fontSize: 12,
                                          fontFamily: "ar",
                                          fontWeight: FontWeight.bold,
                                          color: Colors.grey,
                                        ),
                                      ),
                                      Text(
                                        data['message']['message'].toString(),
                                        textDirection: TextDirection.rtl,
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                          fontSize: 12,
                                          fontFamily: "ar",
                                          fontWeight: FontWeight.bold,
                                          color: Colors.grey,
                                        ),
                                      ),
                                      Text(
                                        data['message']['message'].toString(),
                                        textDirection: TextDirection.rtl,
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                          fontSize: 12,
                                          fontFamily: "ar",
                                          fontWeight: FontWeight.bold,
                                          color: Colors.grey,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                    ]),
                                  )),
                                )
                              ],
                            ),
                          ),
                        )
                      ]));
            })));
  }
}
