import 'package:flutter/material.dart';
import 'package:homecare/backEnd/service.dart';
import 'package:homecare/backEnd/userAuth.dart';
import 'package:homecare/color/myColor.dart';
import 'package:email_validator/email_validator.dart';
import 'package:homecare/model/user.dart';
import 'package:homecare/search.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class Register extends StatelessWidget {
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController nameController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  var signUpKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);

        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
          key: signUpKey,
          backgroundColor: Colors.white,
          body: SafeArea(
            child: Container(
              margin: EdgeInsets.only(left: 30, right: 30, top: 35),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      "assets/login.png",
                      scale: 3.3,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "انشاء حساب جديد",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 20,
                          fontFamily: "ar",
                          color: CustomColor.primaryColor),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      child: TextField(
                        controller: nameController,
                        textAlign: TextAlign.right,
                        decoration: new InputDecoration(
                          contentPadding: EdgeInsets.fromLTRB(13, 13, 0, 13),
                          enabledBorder: const OutlineInputBorder(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(113, 145, 175, 1),
                                width: 1.0),
                          ),
                          focusedBorder: const OutlineInputBorder(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                            borderSide: const BorderSide(
                                color: Colors.grey, width: 0.0),
                          ),
                          hintText: 'الأسم',
                          prefixIcon: Icon(Icons.person,
                              color: Color.fromRGBO(113, 145, 175, 0.5)),
                          hintStyle: TextStyle(
                              fontFamily: "ar",
                              fontSize: 12,
                              color: Color.fromRGBO(113, 145, 175, 1)),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      child: TextField(
                        controller: emailController,
                        keyboardType: TextInputType.emailAddress,
                        textAlign: TextAlign.right,
                        decoration: new InputDecoration(
                          contentPadding: EdgeInsets.fromLTRB(13, 13, 0, 13),
                          enabledBorder: const OutlineInputBorder(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(113, 145, 175, 1),
                                width: 1.0),
                          ),
                          focusedBorder: const OutlineInputBorder(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                            borderSide: const BorderSide(
                                color: Colors.grey, width: 0.0),
                          ),
                          hintText: 'البريد الإلكترونى',
                          prefixIcon: Icon(Icons.email,
                              color: Color.fromRGBO(113, 145, 175, 0.5)),
                          hintStyle: TextStyle(
                              fontFamily: "ar",
                              fontSize: 12,
                              color: Color.fromRGBO(113, 145, 175, 1)),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      // height: 50,
                      child: TextField(
                        obscureText: true,
                        controller: passwordController,
                        textDirection: TextDirection.rtl,
                        textAlign: TextAlign.right,
                        decoration: new InputDecoration(
                            contentPadding: EdgeInsets.fromLTRB(13, 13, 0, 13),
                            enabledBorder: const OutlineInputBorder(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(8)),
                              borderSide: const BorderSide(
                                  color: Color.fromRGBO(113, 145, 175, 1),
                                  width: 1.0),
                            ),
                            focusedBorder: const OutlineInputBorder(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(8)),
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 0.0),
                            ),
                            hintText: 'كلمة المرور',
                            prefixIcon: Icon(Icons.lock_open,
                                color: Color.fromRGBO(113, 145, 175, 0.5)),
                            hintStyle: TextStyle(
                                fontFamily: "ar",
                                fontSize: 12,
                                color: Color.fromRGBO(113, 145, 175, 1))),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      child: TextField(
                        controller: phoneController,
                        keyboardType: TextInputType.phone,
                        textAlign: TextAlign.right,
                        decoration: new InputDecoration(
                          contentPadding: EdgeInsets.fromLTRB(13, 13, 0, 13),
                          enabledBorder: const OutlineInputBorder(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(113, 145, 175, 1),
                                width: 1.0),
                          ),
                          focusedBorder: const OutlineInputBorder(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                            borderSide: const BorderSide(
                                color: Colors.grey, width: 0.0),
                          ),
                          hintText: 'رقم التليفون',
                          prefixIcon: Icon(Icons.phone_android,
                              color: Color.fromRGBO(113, 145, 175, 0.5)),
                          hintStyle: TextStyle(
                              fontFamily: "ar",
                              fontSize: 12,
                              color: Color.fromRGBO(113, 145, 175, 1)),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    InkWell(
                      child: Container(
                          decoration: new BoxDecoration(
                            color: Color.fromRGBO(107, 195, 174, 1),
                            borderRadius: new BorderRadius.all(
                              const Radius.circular(8.0),
                            ),
                          ),
                          height: 48,
                          child: Center(
                            child: Text(
                              "تسجيل الدخول",
                              style: TextStyle(
                                  fontFamily: "ar", color: Colors.white),
                            ),
                          )),
                      onTap: () async {
                        if (emailController.text.isEmpty ||
                            passwordController.text.isEmpty ||
                            nameController.text.isEmpty ||
                            phoneController.text.isEmpty) {
                          showErrorMsgSnackBar(' !من فضلك املأ جميع الحقول');
                        } else if (!EmailValidator.validate(
                            emailController.text)) {
                          showErrorMsgSnackBar(
                              'من فضلك ادخل البريد الألكتروني بصيغة صحيحة');
                        } else {
                          //SignUp

                          User user = User(
                              mail: emailController.text,
                              mobile: phoneController.text,
                              name: nameController.text,
                              password: passwordController.text);

                          showDialog(
                              context: context,
                              barrierDismissible: false,
                              builder: (context) {
                                return AlertDialog(
                                  content: Row(
                                    textDirection: TextDirection.rtl,
                                    children: <Widget>[
                                      CircularProgressIndicator(
                                        valueColor:
                                            new AlwaysStoppedAnimation<Color>(
                                                CustomColor.primaryColor),
                                      ),
                                      SizedBox(
                                        width: 20,
                                      ),
                                      Text(
                                        'جاري تسجيل الدخول',
                                        style: TextStyle(
                                            color: CustomColor.primaryColor,
                                            fontFamily: 'ar',
                                            fontSize: 12),
                                      )
                                    ],
                                  ),
                                );
                              });

                          await userSignUp(user).then((v) async {
                            if (v is User) {

                               //FCM
                                        String fcmToken=await _firebaseMessaging.getToken();
                                        await sendToken(fcmToken,v.token);
                                      
                             // showErrorMsgSnackBar('تم تسجيل المستخدم');
                               Navigator.push(context,MaterialPageRoute(
                                        builder: (context)=>Search(
                                          userName: user.name,
                                          userToken: user.token,
                                        )
                                      ));
                            } else {
                              Navigator.pop(context);
                              print(v);
                              showErrorMsgSnackBar(v);
                            }
                          }).catchError((e) {
                            showErrorMsgSnackBar(e.toString());
                          }).whenComplete(() {
                          //  Navigator.pop(context);
                          });
                        }
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            height: 1,
                            color: Color.fromRGBO(113, 145, 175, 1),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                  color: Color.fromRGBO(113, 145, 175, 1))),
                          child: CircleAvatar(
                            radius: 20,
                            backgroundColor: Colors.transparent,
                            child: Text(
                              'أو',
                              style: TextStyle(
                                color: Color.fromRGBO(113, 145, 175, 1),
                                fontFamily: 'ar',
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            height: 1,
                            color: Color.fromRGBO(113, 145, 175, 1),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            height: 50,
                            child: RaisedButton(
                                onPressed: () async {
                                  await loginFb().then((v) async {
                                    if (v is String) {
                                      showErrorMsgSnackBar(v);
                                    } else {
                                  /*    showErrorMsgSnackBar(
                                          'تم تسجي ل الدخول بنجاح');
                                    */  User user = v;
                                      print(user.toMap());
     //FCM
                                        String fcmToken=await _firebaseMessaging.getToken();
                                        await sendToken(fcmToken,v.token);
                          
                                      Navigator.push(context,MaterialPageRoute(
                                        builder: (context)=>Search(
                                          userName: user.name,
                                          userToken: user.token,
                                        )
                                      ));
                                          
                                    }

                                 
                                 
                                  }).catchError((e) {
                                    print('ErrorLog:$e');
                                    showErrorMsgSnackBar(e.toString());
                                  }).whenComplete(() {
                                    // Navigator.pop(context);
                                  });
                                },
                                color: Color.fromRGBO(59, 89, 151, 1),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                child: Text(
                                  "فيسبوك",
                                  style: TextStyle(
                                      fontFamily: "ar", color: Colors.white),
                                )),
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Expanded(
                          child: Container(
                            height: 50,
                            child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                onPressed: () async {
                                  await googleLogin().then((v) async {
                                    if (v is User) {
                                           //FCM
                                        String fcmToken=await _firebaseMessaging.getToken();
                                        await sendToken(fcmToken,v.token);
                          
                                     /* showErrorMsgSnackBar(
                                          'تم تسجيل الدخول بنجاح');
                                       */    Navigator.push(context,MaterialPageRoute(
                                        builder: (context)=>Search(
                                          userName: v.name,
                                          userToken: v.token,
                                        )
                                      ));
                                    }
                                  }).catchError((e) {
                                    showErrorMsgSnackBar(e.toString());
                                  });
                                },
                                color: Color.fromRGBO(234, 67, 53, 1),
                                child: Text(
                                  "جوجل",
                                  style: TextStyle(
                                      fontFamily: "ar", color: Colors.white),
                                )),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    )
                  ],
                ),
              ),
            ),
          )),
    );
  }

  showErrorMsgSnackBar(String msg) {
    signUpKey.currentState.showSnackBar(SnackBar(
      content: Text(
        msg,
        style: TextStyle(fontFamily: 'ar', fontSize: 12),
        textAlign: TextAlign.right,
      ),
    ));
  }
}
/*
class Register extends StatefulWidget {
  const Register({Key key}) : super(key: key);
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  var data;
  String userEmail;
  Map valueMap;
  String userPassword;
  String error = "";
  String body;
  String service;
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController nameController = new TextEditingController();
  TextEditingController mobileController = new TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  Future<String> apiRequest(String url, Map jsonMap) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.add(utf8.encode(json.encode(jsonMap)));
    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    return reply;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(builder: (context, user, _) {
      return Scaffold(
          body: Container(
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 104,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    InkWell(
                      child: Icon(
                        Icons.chevron_left,
                        color: Color.fromRGBO(48, 83, 121, 1),
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    Image.asset(
                      "assets/register.png",
                      width: MediaQuery.of(context).size.width / 3,
                    ),
                    Container()
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                "انشئ حساب جديد",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 20,
                    fontFamily: "ar",
                    color: Color.fromRGBO(58, 95, 126, 1)),
              ),
              SizedBox(
                height: 40,
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.8,
                child: TextField(
                  controller: nameController,
                  textDirection: TextDirection.rtl,
                  textAlign: TextAlign.right,
                  decoration: new InputDecoration(
                      contentPadding:
                          EdgeInsets.only(top: 13, bottom: 13, left: 30),
                      enabledBorder: const OutlineInputBorder(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(8)),
                        borderSide: const BorderSide(
                            color: Color.fromRGBO(113, 145, 175, 1),
                            width: 1.0),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(8)),
                        borderSide:
                            const BorderSide(color: Colors.grey, width: 1.0),
                      ),
                      hintText: 'الاسم',
                      prefixIcon: Icon(Icons.person,
                          color: Color.fromRGBO(113, 145, 175, 0.5)),
                      hintStyle: TextStyle(
                          fontFamily: "ar",
                          fontSize: 12,
                          color: Color.fromRGBO(113, 145, 175, 0.5))),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.8,
                child: TextField(
                  controller: emailController,
                  textDirection: TextDirection.rtl,
                  textAlign: TextAlign.right,
                  decoration: new InputDecoration(
                      contentPadding:
                          EdgeInsets.only(top: 13, bottom: 13, left: 30),
                      enabledBorder: const OutlineInputBorder(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(8)),
                        borderSide: const BorderSide(
                            color: Color.fromRGBO(113, 145, 175, 1),
                            width: 1.0),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(8)),
                        borderSide:
                            const BorderSide(color: Colors.grey, width: 1.0),
                      ),
                      hintText: 'البريد الإلكترونى',
                      prefixIcon: Icon(Icons.email,
                          color: Color.fromRGBO(113, 145, 175, 0.5)),
                      hintStyle: TextStyle(
                          fontFamily: "ar",
                          fontSize: 12,
                          color: Color.fromRGBO(113, 145, 175, 0.5))),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.8,
                child: TextField(
                  obscureText: true,
                  controller: passwordController,
                  textDirection: TextDirection.rtl,
                  textAlign: TextAlign.right,
                  decoration: new InputDecoration(
                      contentPadding:
                          EdgeInsets.only(top: 13, bottom: 13, left: 30),
                      enabledBorder: const OutlineInputBorder(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(8)),
                        borderSide: const BorderSide(
                            color: Color.fromRGBO(113, 145, 175, 1),
                            width: 1.0),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(8)),
                        borderSide:
                            const BorderSide(color: Colors.grey, width: 1.0),
                      ),
                      hintText: 'كلمة المرور',
                      prefixIcon: Icon(Icons.vpn_key,
                          color: Color.fromRGBO(113, 145, 175, 0.5)),
                      hintStyle: TextStyle(
                          fontFamily: "ar",
                          fontSize: 12,
                          color: Color.fromRGBO(113, 145, 175, 0.5))),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.8,
                child: TextField(
                  controller: mobileController,
                  textDirection: TextDirection.rtl,
                  textAlign: TextAlign.right,
                  decoration: new InputDecoration(
                      contentPadding:
                          EdgeInsets.only(top: 13, bottom: 13, left: 30),
                      enabledBorder: const OutlineInputBorder(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(8)),
                        borderSide: const BorderSide(
                            color: Color.fromRGBO(113, 145, 175, 1),
                            width: 1.0),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(8)),
                        borderSide:
                            const BorderSide(color: Colors.grey, width: 1.0),
                      ),
                      hintText: 'رقم التليفون',
                      prefixIcon: Icon(Icons.phone,
                          color: Color.fromRGBO(113, 145, 175, 0.5)),
                      hintStyle: TextStyle(
                          fontFamily: "ar",
                          fontSize: 12,
                          color: Color.fromRGBO(113, 145, 175, 0.5))),
                ),
              ),
              SizedBox(
                height: 60,
              ),
              InkWell(
                  child: Container(
                      decoration: new BoxDecoration(
                        color: Color.fromRGBO(107, 195, 174, 1),
                        borderRadius: new BorderRadius.all(
                          const Radius.circular(8.0),
                        ),
                      ),
                      width: MediaQuery.of(context).size.width * 0.8,
                      height: 50,
                      child: Center(
                        child: Text(
                          "تسجيل الدخول",
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "ar",
                              color: Colors.white),
                        ),
                      )),
                  onTap: () async {
                    setState(() {
                      apiRequest(
                          "https://delivery.digitalharbor.me//api/register?locale=ar",
                          {
                            "email": emailController.text.trim(),
                            "password": passwordController.text.trim(),
                            "mobile": mobileController.text.trim(),
                            "name": nameController.text.trim(),
                          }).then((value) {
                        error = value;
                        valueMap = json.decode(value);

                        if (valueMap["type"] == "client") {
                          user.userName = valueMap["name"];
                          user.userToken = valueMap["token"];

                          //print(valueMap["token"]);
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Search()));
                        } else {
                          showDialog<void>(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                content: const Text(
                                  'اسم المستخدم او كلمة المرور خطاء',
                                  style: TextStyle(fontFamily: "ar"),
                                ),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text(
                                      'اعد التجربه',
                                      style: TextStyle(fontFamily: "ar"),
                                    ),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ],
                              );
                            },
                          );
                        }
                      });
                    });
                  }),
              SizedBox(
                height: 30,
              ),
              Stack(
                children: <Widget>[
                  Positioned(
                    top: 20,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 20, left: 20),
                      child: Container(
                        color: Color.fromRGBO(113, 145, 175, 1),
                        height: 1,
                        width: MediaQuery.of(context).size.width * 0.9,
                      ),
                    ),
                  ),
                  Center(
                    child: Container(
                      width: 41.0,
                      height: 41.0,
                      decoration: new BoxDecoration(
                        border:
                            Border.all(color: Color.fromRGBO(113, 145, 175, 1)),
                        color: Colors.white,
                        shape: BoxShape.circle,
                      ),
                      child: Center(
                          child: Text(
                        "او",
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: "rp",
                            color: Color.fromRGBO(113, 145, 175, 1)),
                      )),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                      decoration: new BoxDecoration(
                        color: Color.fromRGBO(59, 89, 151, 1),
                        borderRadius: new BorderRadius.all(
                          const Radius.circular(8.0),
                        ),
                      ),
                      width: MediaQuery.of(context).size.width * 0.42,
                      height: 48,
                      child: Center(
                        child: Text(
                          "فيسبوك",
                          style: TextStyle(
                              fontSize: 16,
                              fontFamily: "ar",
                              color: Colors.white),
                        ),
                      )),
                  SizedBox(
                    width: 19.5,
                  ),
                  Container(
                      decoration: new BoxDecoration(
                        color: Color.fromRGBO(234, 67, 53, 1),
                        borderRadius: new BorderRadius.all(
                          const Radius.circular(8.0),
                        ),
                      ),
                      width: MediaQuery.of(context).size.width * 0.42,
                      height: 48,
                      child: Center(
                        child: Text(
                          "جوجل",
                          style: TextStyle(
                              fontSize: 16,
                              fontFamily: "ar",
                              color: Colors.white),
                        ),
                      )),
                ],
              ),
              SizedBox(
                height: 40,
              ),
            ],
          ),
        ),
      ));
    });
  }
}
*/
