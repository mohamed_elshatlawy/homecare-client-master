import 'package:flutter/material.dart';

class Splash extends StatelessWidget {
  const Splash({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          child: Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                "assets/logo.png",
                width: MediaQuery.of(context).size.width / 3,
              ),
              SizedBox(
                height: 50,
              ),
              Text(
                "HomeCare V0.1",
                style: TextStyle(color: Color.fromRGBO(107, 195, 174, 1)),
              )
            ],
          )),
        ),
      ),
    );
  }
}
