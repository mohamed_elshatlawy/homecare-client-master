import 'package:flutter/material.dart';
import 'package:homecare/access/login.dart';
import 'package:homecare/color/myColor.dart';
import 'package:dots_indicator/dots_indicator.dart';

import '../terms.dart';

class Intro extends StatefulWidget {
  @override
  _IntroState createState() => _IntroState();
}

int index = 0;
PageController pcontroller = new PageController(initialPage: 0);

String lang = "en";

class _IntroState extends State<Intro> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Center(
            child: Container(
            
              height: 400,
              margin: EdgeInsets.only(left: 20, right: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Expanded(
                    flex: 4,
                    child: Container(
                     
                     
                      child: PageView(
                        onPageChanged: (i) {
                          setState(() {
                            index = i;
                          });
                        },
                        controller: pcontroller,
                        children: <Widget>[
                          Container(
                            child: Column(
                              children: <Widget>[
                                Image.asset(
                                  "assets/logo.png",
                                  width: 289,
                                  height: 200,
                                ),
                                SizedBox(
                                  height: 50,
                                ),
                                Text(
                                  "الخدمات كما كنت تتمنى",
                                  style: TextStyle(
                                      fontFamily: "ar",
                                      fontSize: 20,
                                      color: CustomColor.primaryColor,
                                      fontWeight: FontWeight.w600),
                                ),
                                Text(
                                  'هل انت تبحث عن المساعدة ؟',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: "rb",
                                      fontSize: 14,
                                      color: CustomColor.thirdColor,
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ),
                   
                          Container(
                            child: Column(
                              children: <Widget>[
                                Image.asset(
                                  "assets/intro2.png",
                                  width: 289,
                                  height: 200,
                                ),
                                SizedBox(height: 30,),
                                Text(
                                  "خدمات متميزة",
                                  style: TextStyle(
                                      fontFamily: "ar",
                                      fontSize: 20,
                                      color: CustomColor.primaryColor,
                                      fontWeight: FontWeight.w600),
                                ),
                                Text(
                                 
                                 'لن تضطر إلي البحث في أي مكان أخر للقيام بكل خدمات الصيانة الخاصة بك. سوف نقوم بها جميعاً من أجلك!',
                                 textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: "rb",
                                      fontSize: 14,
                                      color: CustomColor.thirdColor,
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ),
                       
                          Container(
                            child: Column(
                              children: <Widget>[
                                Image.asset(
                                  "assets/intro1.png",
                                  width: 289,
                                  height: 200,
                                ),
                                SizedBox(height: 30,),
                                Text(
                                  "كن متواصل دائما",
                                  style: TextStyle(
                                      fontFamily: "ar",
                                      fontSize: 20,
                                      color: CustomColor.primaryColor,
                                      fontWeight: FontWeight.w600),
                                ),
                                Text(
                                  'تنظيف منزلك, مكافحة الحشرات المنزلية أو صيانة الأجهزة المنزلية؟  الأمر أصبح أسهل الأن',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: "rb",
                                      fontSize: 14,
                                      color: CustomColor.thirdColor,
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
          
                  Expanded(
                    flex: 1,
                                      child: new DotsIndicator(
                      dotsCount: 3,
                      position: double.parse(index.toString()),
                      decorator: DotsDecorator(
                        color: CustomColor.primaryColor,
                        size: Size(12, 12),
                        activeSize: Size(12, 12),
                        spacing: EdgeInsets.all(4),
                        activeColor: CustomColor.secondartColor,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: EdgeInsets.only(left: 20, right: 20, bottom: 40),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: 50,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      color: CustomColor.primaryColor,
                      textColor: Colors.white,
                      onPressed: () {
                        if (index != 2) {
                          setState(() {
                            index++;
                          });
                          pcontroller.nextPage(
                              curve: Curves.ease,
                              duration: Duration(milliseconds: 500));
                        } else {
                          print('Done');
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) => Login()));
                        }
                      },
                      child: Text(
                        (index == 0)
                            ? 'ابدأ الجولة'
                            : (index == 1) ? 'التالي' : 'ابدأ الان',
                        style: TextStyle(
                            fontFamily: "ar",
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    textDirection: TextDirection.rtl,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "بالاستمرار انت موافق على",
                        style: TextStyle(
                            fontFamily: "ar",
                            fontSize: 12,
                            color: CustomColor.primaryColor,
                            fontWeight: FontWeight.w300),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).push(
                              MaterialPageRoute(builder: (context) => Terms()));
                        },
                        child: Text(
                          "شروط وسياسة الاستخدام",
                          style: TextStyle(
                              fontFamily: "ar",
                              fontSize: 12,
                              color: Color.fromRGBO(107, 195, 174, 1),
                              fontWeight: FontWeight.w300),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
