import 'package:flutter/material.dart';
import 'package:homecare/backEnd/service.dart';
import 'package:homecare/color/myColor.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:homecare/providers/user.dart';
import 'package:provider/provider.dart';

class UpdateInfo extends StatelessWidget {
  String title, fieldName, token;

  TextEditingController fieldController;
  UpdateInfo(this.title, this.fieldController, this.fieldName, this.token);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(251, 251, 251, 1),
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back_ios,
              color: CustomColor.primaryColor,
              size: 20,
            )),
        title: Text(
          'تغيير $title',
          style: TextStyle(
              fontFamily: 'ar', fontSize: 15, color: CustomColor.primaryColor),
        ),
        centerTitle: true,
      ),
      body: Container(
        margin: EdgeInsets.only(left: 20, right: 20),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 40,
            ),
            Row(
              textDirection: TextDirection.rtl,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(color: Colors.grey, fontFamily: 'ar'),
                )
              ],
            ),
            SizedBox(
              height: 15,
            ),
            Container(
              padding: EdgeInsets.only(right: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 4)],
                color: Colors.white,
              ),
              child: TextField(
                controller: fieldController,
                textDirection: TextDirection.rtl,
                style: TextStyle(
                    color: CustomColor.primaryColor, fontFamily: 'ar'),
                decoration: InputDecoration(border: InputBorder.none),
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Container(
              width: double.infinity,
              height: 48,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                onPressed: () async {
                  showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (context) {
                        return AlertDialog(
                          content: Row(
                            textDirection: TextDirection.rtl,
                            children: <Widget>[
                              Text('جاري تحديث البيانات'),
                              SizedBox(
                                width: 15,
                              ),
                              CircularProgressIndicator(
                                valueColor: new AlwaysStoppedAnimation<Color>(
                                    CustomColor.primaryColor),
                              )
                            ],
                          ),
                        );
                      });

                  await updateUserInfo(fieldController.text, fieldName, token)
                      .then((v) {
                    Fluttertoast.showToast(
                        msg: v,
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIos: 1,
                        backgroundColor: CustomColor.primaryColor,
                        textColor: Colors.white,
                        fontSize: 16.0);
                    //    Navigator.pop(context);
                    final user = Provider.of<UserModel>(context);
                    user.userName = fieldController.text;
                  }).whenComplete(() {
                    Navigator.pop(context);
                  });
                },
                child: Text(
                  'حفظ',
                  style: TextStyle(fontFamily: 'ar'),
                ),
                color: CustomColor.secondartColor,
                textColor: Colors.white,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('الغاء',
                  style: TextStyle(fontFamily: 'ar', color: Colors.grey[600])),
            )
          ],
        ),
      ),
    );
  }
}
