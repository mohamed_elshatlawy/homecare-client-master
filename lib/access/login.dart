import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';

import 'package:homecare/access/register.dart';
import 'package:homecare/backEnd/service.dart';
import 'package:homecare/backEnd/userAuth.dart';
import 'package:homecare/color/myColor.dart';
import 'package:homecare/model/user.dart';
import 'package:homecare/providers/user.dart';
import 'package:homecare/search.dart';
import 'package:email_validator/email_validator.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class Login extends StatefulWidget {
  _LoginState createState() => _LoginState();
}

Future<String> apiRequest(String url, Map jsonMap) async {
  HttpClient httpClient = new HttpClient();
  HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
  request.headers.set('content-type', 'application/json');
  request.add(utf8.encode(json.encode(jsonMap)));
  HttpClientResponse response = await request.close();

  String reply = await response.transform(utf8.decoder).join();
  httpClient.close();
  return reply;
}

class _LoginState extends State<Login> {
  String userEmail;
  Map valueMap;
  String userPassword;
  String error = "";
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  bool load = false;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  var loginKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
    
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
          key: loginKey,
          backgroundColor: Colors.white,
          body: SafeArea(
            child: Container(
              margin: EdgeInsets.only(left: 30, right: 30, top: 60),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      "assets/login.png",
                      scale: 3.3,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "ادخل الى حسابك",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 20,
                          fontFamily: "ar",
                          color: CustomColor.primaryColor),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      child: TextField(
                        controller: emailController,
                        keyboardType: TextInputType.emailAddress,
                        textAlign: TextAlign.right,
                        decoration: new InputDecoration(
                          contentPadding: EdgeInsets.fromLTRB(13, 13, 0, 13),
                          enabledBorder: const OutlineInputBorder(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(113, 145, 175, 1),
                                width: 1.0),
                          ),
                          focusedBorder: const OutlineInputBorder(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                            borderSide: const BorderSide(
                                color: Colors.grey, width: 0.0),
                          ),
                          hintText: 'البريد الإلكترونى',
                          prefixIcon: Icon(Icons.email,
                              color: Color.fromRGBO(113, 145, 175, 0.5)),
                          hintStyle: TextStyle(
                              fontFamily: "ar",
                              fontSize: 12,
                              color: Color.fromRGBO(113, 145, 175,1)),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      // height: 50,
                      child: TextField(
                        obscureText: true,
                        controller: passwordController,
                        textDirection: TextDirection.rtl,
                        textAlign: TextAlign.right,
                        decoration: new InputDecoration(
                            contentPadding: EdgeInsets.fromLTRB(13, 13, 0, 13),
                            enabledBorder: const OutlineInputBorder(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(8)),
                              borderSide: const BorderSide(
                                  color: Color.fromRGBO(113, 145, 175, 1),
                                  width: 1.0),
                            ),
                            focusedBorder: const OutlineInputBorder(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(8)),
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 0.0),
                            ),
                            hintText: 'كلمة المرور',
                            prefixIcon: Icon(Icons.lock_open,
                                color: Color.fromRGBO(113, 145, 175, 0.5)),
                            hintStyle: TextStyle(
                                fontFamily: "ar",
                                fontSize: 12,
                                color: Color.fromRGBO(113, 145, 175, 1))),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    InkWell(
                      child: Container(
                          decoration: new BoxDecoration(
                            color: Color.fromRGBO(107, 195, 174, 1),
                            borderRadius: new BorderRadius.all(
                              const Radius.circular(8.0),
                            ),
                          ),
                          height: 48,
                          child: Center(
                            child: Text(
                              "تسجيل الدخول",
                              style: TextStyle(
                                  fontFamily: "ar", color: Colors.white),
                            ),
                          )),
                      onTap: () async {
                        if (emailController.text.isEmpty ||
                            passwordController.text.isEmpty) {
                          showErrorMsgSnackBar(' !من فضلك املأ جميع الحقول');
                        } else if (!EmailValidator.validate(
                            emailController.text)) {
                          showErrorMsgSnackBar(
                              'من فضلك ادخل البريد الألكتروني بصيغة صحيحة');
                        } else {
                          //Login

                          showDialog(
                              context: context,
                              barrierDismissible: false,
                              builder: (context) {
                                return AlertDialog(
                                  content: Row(
                                    textDirection: TextDirection.rtl,
                                    children: <Widget>[
                                      CircularProgressIndicator(
                                        valueColor:
                                            new AlwaysStoppedAnimation<Color>(
                                                CustomColor.primaryColor),
                                      ),
                                      SizedBox(
                                        width: 20,
                                      ),
                                      Text(
                                        'جاري تسجيل الدخول',
                                        style: TextStyle(
                                            color: CustomColor.primaryColor,
                                            fontFamily: 'ar',
                                            fontSize: 12),
                                      )
                                    ],
                                  ),
                                );
                              });

                          await userLogin(
                                  emailController.text, passwordController.text)
                              .then((v) async {
                            if (v is String) {
                              Navigator.pop(context);
                              
                              showErrorMsgSnackBar(v);
                            } else {
                              // showErrorMsgSnackBar('تم تسجيل الدخول بنجاح');
                              SharedPreferences prefs =
                                  await SharedPreferences.getInstance();

                              prefs.setString('name', v.name);
                              prefs.setString('token', v.token);
                                
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Search(
                                            userName: v.name,
                                            userToken: v.token,
                                          )));
                            }
                          }).catchError((e) {
                            print(e);
                            showErrorMsgSnackBar('فشل في الاتصال بالأنترنت');
                          }).whenComplete(() {
                         //   Navigator.pop(context);
                          });
                        }

                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            height: 1,
                            color: Color.fromRGBO(113, 145, 175, 1),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                  color: Color.fromRGBO(113, 145, 175, 1))),
                          child: CircleAvatar(
                            radius: 20,
                            backgroundColor: Colors.transparent,
                            child: Text(
                              'أو',
                              style: TextStyle(
                                color: Color.fromRGBO(113, 145, 175, 1),
                                fontFamily: 'ar',
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            height: 1,
                            color: Color.fromRGBO(113, 145, 175, 1),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            height: 50,
                            child: RaisedButton(
                                onPressed: () async {
                                  print('fbLoging!!!');

                                  await loginFb().then((v) async {
                                    if (v is String) {
                                      showErrorMsgSnackBar(v);
                                    } else {
                                      showErrorMsgSnackBar(
                                          'تم تسجي ل الدخول بنجاح');
                                             SharedPreferences prefs =
                                  await SharedPreferences.getInstance();

                              prefs.setString('name', v.name);
                              prefs.setString('token', v.token);

                               //FCM
                                        String fcmToken=await _firebaseMessaging.getToken();
                                        await sendToken(fcmToken,v.token);
                                      
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => Search(
                                                    userName: v.name,
                                                    userToken: v.token,
                                                  )));
                                      User user = v;
                                      print(user.toMap());
                                    }
                                  }).catchError((e) {
                                    print('ErrorLog:$e');
                                    showErrorMsgSnackBar(e.toString());
                                  }).whenComplete(() {
                                    // Navigator.pop(context);
                                  });
                                },
                                color: Color.fromRGBO(59, 89, 151, 1),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                child: Text(
                                  "فيسبوك",
                                  style: TextStyle(
                                      fontFamily: "ar", color: Colors.white),
                                )),
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Expanded(
                          child: Container(
                            height: 50,
                            child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                onPressed: () async {
                                  await googleLogin().then((v) async {
                                    if (v is User) {
                                      SharedPreferences prefs =
                                          await SharedPreferences.getInstance();

                                      prefs.setString('name', v.name);
                                      prefs.setString('token', v.token);
                                      //FCM
                                        String fcmToken=await _firebaseMessaging.getToken();
                                        await sendToken(fcmToken,v.token);

                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => Search(
                                                    userName: v.name,
                                                    userToken: v.token,
                                                  )));
                                    }
                                  }).catchError((e) {
                                    showErrorMsgSnackBar(e.toString());
                                  });
                                },
                                color: Color.fromRGBO(234, 67, 53, 1),
                                child: Text(
                                  "جوجل",
                                  style: TextStyle(
                                      fontFamily: "ar", color: Colors.white),
                                )),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        InkWell(
                          child: Text(
                            "انشاء حساب جديد",
                            style: TextStyle(
                              fontSize: 13,
                              fontFamily: "ar",
                              color: Color.fromRGBO(107, 195, 174, 1),
                            ),
                          ),
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Register()));
                          },
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text("ليس لديك حساب؟ ",
                            style: TextStyle(
                                fontSize: 13,
                                fontFamily: "ar",
                                color: Color.fromRGBO(48, 83, 121, 1))),
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    )
                  ],
                ),
              ),
            ),
          )),
    );
  }

  showErrorMsgSnackBar(String msg) {
    loginKey.currentState.showSnackBar(SnackBar(
      content: Text(
        msg,
        style: TextStyle(fontFamily: 'ar', fontSize: 13),
        textAlign: TextAlign.right,
      ),
    ));
  }
}
