import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:homecare/access/updateInfo.dart';
import 'package:homecare/backEnd/service.dart';
import 'package:homecare/color/myColor.dart';
import 'package:homecare/home.dart';
import 'package:homecare/menu.dart';
import 'package:homecare/providers/switchProvider.dart';
import 'package:homecare/providers/user.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:image_picker/image_picker.dart';

class Account extends StatefulWidget {
  final String category;
  Account({this.category, Key key}) : super(key: key);
  _AccountState createState() => _AccountState();
}

class _AccountState extends State<Account> {
  var data;
  TextEditingController nameController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController mobileController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  bool switchBtn = false;

  String imgURL = "";

  Future<String> getData() async {
    final user = Provider.of<UserModel>(context);

    var response = await http.get(
        Uri.encodeFull("http://delivery.digitalharbor.me//api/profile"),
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer " + user.userToken,
        });
    setState(() {
      data = json.decode(response.body);
      nameController.text = data['name'];
      emailController.text = data['email'];
      mobileController.text = data['mobile'];
      imgURL = data['image_link'];
      user.userName=nameController.text;
    });

    return "Success";
  }

  bool editName = false, editMail = false, editPhone = false;

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      getData();
    });
    super.initState();
  }

  bool switchKey = false;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    //update profile
    final user = Provider.of<UserModel>(context);
    print('token:${user.userToken}');
    updateUserImage(image, user.userToken).then((v) {
      getData();
      print('DoneUserUpdateImg:$v');
    }).catchError((e) {
      print('ErrorUpdateUserProfile:$e');
    });
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>(); // ADD THIS LINE

    return Consumer<UserModel>(builder: (context, user, _) {
      return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            leading: InkWell(
                onTap: () {
                  Navigator.pushReplacement(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: Icon(
                  Icons.arrow_back_ios,
                  color: CustomColor.primaryColor,
                  size: 20,
                )),
            title: Text(
              'حسابي',
              style: TextStyle(
                  fontFamily: 'ar',
                  fontSize: 15,
                  color: CustomColor.primaryColor),
            ),
            centerTitle: true,
          ),
          body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  SizedBox(
                    height: 40,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                              width: 100,
                              height: 100,
                              child: Stack(
                                fit: StackFit.expand,
                                children: <Widget>[
                                  Center(
                                    child: Container(
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          border: Border.all(
                                              color: CustomColor.primaryColor,
                                              width: 2.5)),
                                      child: CircleAvatar(
                                        radius: 80,
                                        backgroundImage: (imgURL == "")
                                            ? AssetImage(
                                                'assets/profile.png',
                                              )
                                            : NetworkImage(imgURL),
                                        backgroundColor: Colors.white,
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.bottomRight,
                                    child: InkWell(
                                      onTap: () {
                                        getImage();
                                      },
                                      child: CircleAvatar(
                                        radius: 15,
                                        backgroundColor:
                                            CustomColor.primaryColor,
                                        child: Image.asset(
                                          'assets/writing.png',
                                          scale: 2.3,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              )),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        user.userName,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 18, color: Colors.black),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 20, left: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          "المعلومات الشخصية",
                          textAlign: TextAlign.right,
                          style: TextStyle(
                              fontSize: 18,
                              fontFamily: "ar",
                              color: Color.fromRGBO(58, 95, 126, 1)),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Divider(),
                        Text(
                          "الاسم",
                          textAlign: TextAlign.right,
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "ar",
                              color: Colors.grey),
                        ),
                        Container(
                          height: 40,
                          child: Row(
                            children: <Widget>[
                              InkWell(
                                onTap: () async {
                                  await Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => UpdateInfo(
                                            'الأسم',
                                            nameController,
                                            'name',
                                            user.userToken),
                                      ));
                                  setState(() {});
                                },
                                child: Image.asset(
                                  'assets/writing.png',
                                  color: (editName)
                                      ? CustomColor.primaryColor
                                      : Colors.grey,
                                  scale: 2,
                                ),
                              ),
                              Expanded(
                                child: TextField(
                                  enabled: editName,
                                  decoration:
                                      InputDecoration(border: InputBorder.none),
                                  controller: nameController,
                                  textDirection: TextDirection.rtl,
                                  textAlign: TextAlign.right,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Divider(),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          "البريد الإلكتروني",
                          textAlign: TextAlign.right,
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "ar",
                              color: Colors.grey),
                        ),
                        Container(
                          height: 40,
                          child: Row(
                            children: <Widget>[
                              InkWell(
                                onTap: () async {},
                                child: Image.asset(
                                  'assets/writing.png',
                                  color: (editMail)
                                      ? CustomColor.primaryColor
                                      : Colors.grey,
                                  scale: 2,
                                ),
                              ),
                              Expanded(
                                child: TextField(
                                  enabled: false,
                                  style: TextStyle(color: Colors.grey),
                                  decoration:
                                      InputDecoration(border: InputBorder.none),
                                  controller: emailController,
                                  textDirection: TextDirection.rtl,
                                  textAlign: TextAlign.right,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Divider(),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          "رقم التليفون",
                          textAlign: TextAlign.right,
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "ar",
                              color: Colors.grey),
                        ),
                        Container(
                          height: 40,
                          child: Row(
                            children: <Widget>[
                              InkWell(
                                onTap: () async {
                                  await Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => UpdateInfo(
                                            'رقم التليفون',
                                            mobileController,
                                            'mobile',
                                            user.userToken),
                                      ));
                                  setState(() {});
                                },
                                child: Image.asset(
                                  'assets/writing.png',
                                  color: (editPhone)
                                      ? CustomColor.primaryColor
                                      : Colors.grey,
                                  scale: 2,
                                ),
                              ),
                              Expanded(
                                child: TextField(
                                  enabled: editPhone,
                                  decoration:
                                      InputDecoration(border: InputBorder.none),
                                  controller: mobileController,
                                  textDirection: TextDirection.rtl,
                                  textAlign: TextAlign.right,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Divider(),
                        SizedBox(
                          height: 8,
                        ),
                        SizedBox(
                          height: 40,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          "اعدادات التطبيق",
                          textAlign: TextAlign.right,
                          style: TextStyle(
                              fontSize: 18,
                              fontFamily: "ar",
                              color: Color.fromRGBO(58, 95, 126, 1)),
                        ),
                        Row(
                          textDirection: TextDirection.rtl,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('التنبيهات',
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Colors.black54,
                                  fontFamily: "ar",
                                )),
                            Switch(
                              onChanged: (v) {
                                Provider.of<SwitchProvider>(context)
                                    .switchNotification(v);
                              },
                              activeColor: CustomColor.primaryColor,
                              value: Provider.of<SwitchProvider>(context)
                                  .switchValue,
                            )
                          ],
                        ),
                        Divider(),
                        Text(
                          "تسجيل خروج",
                          textAlign: TextAlign.right,
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "ar",
                              color: CustomColor.secondartColor),
                        ),
                        SizedBox(
                          height: 40,
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
