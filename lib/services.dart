import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:homecare/color/myColor.dart';
import 'package:homecare/dna/changeLocation.dart';
import 'package:homecare/home.dart';
import 'package:homecare/menu.dart';
import 'package:homecare/order/map.dart';
import 'package:homecare/providers/user.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import 'package:place_picker/place_picker.dart';

class Services extends StatefulWidget {
  final String category;
  Services({this.category, Key key}) : super(key: key);
  _ServicesState createState() => _ServicesState();
}

class _ServicesState extends State<Services> {
  var data;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      getData();
    });
  }

  Future<String> getData() async {
    final user = Provider.of<UserModel>(context);

    var response = await http.get(
        Uri.encodeFull("http://delivery.digitalharbor.me//api/category/" +
            widget.category +
            "?locale=ar&district_id=" +
            user.locationID),
        headers: {"Accept": "application/json"});
    setState(() {
      data = json.decode(response.body);
    });
    return "Success";
  }

  Widget getList() {
    if (data == null) {
      return Container(
        height: MediaQuery.of(context).size.height * 0.8,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    if (data.length < 1) {
      return Container(
        height: MediaQuery.of(context).size.height * 0.8,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: Text('لا توجد خدمات'),
        ),
      );
    }
    return Container(
      height: MediaQuery.of(context).size.height - 152,
      width: MediaQuery.of(context).size.width,
      color: Colors.white,
      child: ListView.builder(
        padding: EdgeInsets.only(top: 10),
        itemCount: data?.length,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 5),
            child: getListItem(index),
          );
        },
      ),
    );
  }

  Widget getListItem(int i) {
    if (data == null || data.length < 1) return null;

    return InkWell(
      child: Container(
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
              color: Color.fromRGBO(48, 30, 104, 0.15),
              blurRadius: 18,
              spreadRadius: 0)
        ]),
        height: 120,
        width: MediaQuery.of(context).size.width * 0.9,
        child: Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          child: Padding(
            padding: EdgeInsets.only(left: 10, top: 13, bottom: 13),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.64,
                  child: Column(
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                data[i]['price'].toString() + " LE",
                                style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: "ar",
                                  fontWeight: FontWeight.w900,
                                  color: Color.fromRGBO(100, 100, 100, 1),
                                ),
                              ),
                              Text(
                                data[i]['title'].toString(),
                                textAlign: TextAlign.right,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: "ar",
                                  fontWeight: FontWeight.bold,
                                  color: Color.fromRGBO(48, 83, 121, 1),
                                ),
                              ),
                            ],
                          ),
                          RatingBar(
                            initialRating: data[i]['rating'].toDouble(),
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            ignoreGestures: true,
                            itemCount: 5,
                            itemSize: 15,
                            itemPadding: EdgeInsets.symmetric(horizontal: 0.8),
                            itemBuilder: (context, _) => Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                            onRatingUpdate: (rating) {
                              // print(rating);
                            },
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 6.5,
                      ),
                      Flexible(
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          child: RichText(
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.right,
                            textDirection: TextDirection.rtl,
                            strutStyle: StrutStyle(fontSize: 12.0),
                            text: TextSpan(
                                style: TextStyle(
                                  fontSize: 12,
                                  fontFamily: "rb",
                                  color: Color.fromRGBO(158, 158, 158, 1),
                                ),
                                text: data[i]['description'].toString()),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(right: 10, left: 5),
                  width: MediaQuery.of(context).size.width * 0.24,
                  height: MediaQuery.of(context).size.width * 0.27,
                  child: new ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: FadeInImage.assetNetwork(
                        placeholder: 'assets/service1.png',
                        image: data[i]['image_link'].toString(),
                        fit: BoxFit.fill,
                      )),
                ),
              ],
            ),
          ),
        ),
      ),
      onTap: () async {
        var result = await Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => PlacePicker(
                  "AIzaSyArqxjb1ObPE7H-KcaCr1RExwCLnajHDcU",
                )));

      print(result);
        if(result!=null)
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => OrderMap(
                      title: data[i]['title'].toString(),
                      serviceID: data[i]['id'].toString(),
                      description: data[i]['description'].toString(),
                      price: data[i]['price'].toString(),
                      result: result,
                    )));
     
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>(); // ADD THIS LINE

    return Consumer<UserModel>(builder: (context, user, _) {
      return Scaffold(
        key: _scaffoldKey,
        endDrawer: Drawer(child: Menu()),
           appBar: PreferredSize(
                        child: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          leading: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(
                  Icons.arrow_back_ios,
                  color: CustomColor.primaryColor,
                  size: 20,
                )),
          title: Text(
              'الخدمات',
              style: TextStyle(
                  fontFamily: 'ar',
                  fontSize: 15,
                  color: CustomColor.primaryColor),
          ),
          centerTitle: true,
          actions: <Widget>[
              InkWell(
                child: Container(
                  child: Image.asset(
                    "assets/menu.png",
                    width: 25,
                    height: 18.3,
                  ),
                ),
                onTap: () {
                  _scaffoldKey.currentState.openEndDrawer();
                },
              ),
              SizedBox(
                width: 15,
              )
          ],
        ), preferredSize: Size.fromHeight(60),
           ),
      
        body: Container(
          child: Column(
            children: <Widget>[
           
              ChangeLocation(),
              Expanded(child: getList())
            ],
          ),
        ),
      );
    });
  }
}
